find_program(GLSLANG_VALIDATOR glslangValidator)

function(target_add_spirv_shaders TARGET_NAME DESTINATION_DIR)
	foreach(SHADER ${ARGN})
		get_filename_component(FILE_NAME ${SHADER} NAME)
		if (NOT IS_ABSOLUTE SHADER)
			set(SHADER "${CMAKE_CURRENT_SOURCE_DIR}/${SHADER}")
		endif()
		set(SPIRV "${DESTINATION_DIR}/${FILE_NAME}.spv")
		add_custom_command(
			OUTPUT ${SPIRV}
			COMMAND ${CMAKE_COMMAND} -E make_directory "${DESTINATION_DIR}/"
			COMMAND ${GLSLANG_VALIDATOR} -V ${SHADER} -o ${SPIRV}
			DEPENDS ${SHADER})
		list(APPEND SPIRV_BINARY_FILES ${SPIRV})
	endforeach(SHADER)
	add_custom_target(${TARGET_NAME}_shaders DEPENDS ${SPIRV_BINARY_FILES})
	add_dependencies(${TARGET_NAME} ${TARGET_NAME}_shaders)
endfunction(target_add_spirv_shaders)