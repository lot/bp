#include <bp_glfw/bp_glfw.hpp>
#include <bp/gpu/instance.hpp>
#include <bp/gpu/context.hpp>
#include <bp/gpu/gpu_query.hpp>
#include <bp/gpu/gpu_error.hpp>
#include <bp/gpu/swapchain.hpp>
#include <bp/gpu/render_pass.hpp>
#include <bp/gpu/attachment.hpp>
#include <bp/gpu/subpass.hpp>
#include <bp/gpu/framebuffer.hpp>
#include <bp/gpu/batch.hpp>
#include <bp/gpu/render_pass_instance.hpp>
#include <bp/gpu/subpass_instance.hpp>
#include <bp/gpu/draw_object.hpp>
#include <bp/gpu/shader.hpp>
#include <bp/gpu/pipeline_layout.hpp>
#include <bp/gpu/pipeline.hpp>
#include <bp/gpu/buffer.hpp>
#include <bp/util/fileutil.hpp>
#include <bp/util/event.hpp>
#include <iostream>

using namespace std;

struct window_events {
	bp::event<uint32_t, uint32_t> resize;
};

void run(const string& shader_path, GLFWwindow* window, window_events& events) {
	auto instance = bp_glfw::create_instance();
	auto surface = bp_glfw::create_surface(instance, window);
	bp::gpu_usage gpu_usage = bp::gpu_usage::graphics | bp::gpu_usage::surface;

	auto query = instance->create_gpu_query();
	query->usage(gpu_usage);
	query->surface(surface);

	auto gpus = query->results();
	if (gpus.empty()) {
		throw bp::gpu_error{
			"No suitable GPU."
		};
	}

	cout << "Suitable GPUs:" << endl;
	for (bp::gpu& gpu : gpus)
		cout << '\t' << gpu.name() << endl;

	auto context =
		instance->create_context(gpus[0], gpu_usage);

	auto vbuffer = context->create_buffer(
		bp::buffer_usage::vertex,
		sizeof(float) * 6
	);
	{
		float* buffer_data = reinterpret_cast<float*>(vbuffer->map());
		buffer_data[0] = 0.f;
		buffer_data[1] = -0.5f;
		buffer_data[2] = -0.5f;
		buffer_data[3] = 0.5f;
		buffer_data[4] = 0.5f;
		buffer_data[5] = 0.5f;
		vbuffer->unmap();
	}

	auto upload_batch = context->create_batch();
	upload_batch->upload(vbuffer);
	auto upload_job = upload_batch->execute();

	auto swapchain = context->create_swapchain(
		surface,
		bp::swapchain_flags::vsync,
		640, 480
	);

	auto depth_buffer = context->create_texture(
		bp::texture_type::texture_2D,
		bp::texture_format::depth,
		bp::texture_usage::attachment,
		640, 480
	);

	auto render_pass = context->create_render_pass();
	auto color_attachment = render_pass->add_attachment(
		swapchain,
		bp::attachment_flags::clear | bp::attachment_flags::store
	);
	color_attachment->clear_color(0.5f, 0.5f, 0.5f, 1.f);
	auto depth_attachment = render_pass->add_attachment(
		bp::texture_format::depth,
		bp::attachment_flags::clear
	);
	auto subpass = render_pass->add_subpass();
	subpass->add_color_attachment(color_attachment);
	subpass->set_depth_attachment(depth_attachment);

	auto framebuffer = context->create_framebuffer(render_pass, 640, 480);
	framebuffer->bind(color_attachment, swapchain);
	framebuffer->bind(depth_attachment, depth_buffer);

	auto code = bp::load_binary_file(
		bp::path(shader_path, "test.vert.spv")
	);
	auto vs = context->create_shader(
		bp::shader_stage::vertex,
		code.size(),
		code.data()
	);

	code = bp::load_binary_file(
		bp::path(shader_path, "test.frag.spv")
	);
	auto fs = context->create_shader(
		bp::shader_stage::fragment,
		code.size(),
		code.data()
	);

	auto pipeline_layout = context->create_pipeline_layout();
	pipeline_layout->add_push_constant_range(
		{bp::shader_stage::fragment},
		0, sizeof(float) * 4
	);
	auto pipeline = context->create_graphics_pipeline(
		pipeline_layout,
		{vs, fs},
		subpass,
		{{0, sizeof(float) * 2, bp::input_rate::vertex}},
		{{0, 0, bp::shader_data_type::vec2, 0}},
		bp::primitive_topology::triangle_list,
		bp::polygon_mode::fill,
		bp::graphics_pipeline_flags::depth_test
		| bp::graphics_pipeline_flags::cull_back_face
	);

	auto batch = context->create_batch();
	auto render_pass_instance = batch->render_pass(
		render_pass,
		framebuffer,
		0, 0,
		640, 480
	);

	float frag_color[4] = {0.f, 1.f, 0.f, 1.f};
	auto subpass_instance = render_pass_instance->subpass(subpass);
	auto draw = subpass_instance->add_draw_object();
	draw->bind_pipeline(pipeline);
	draw->push_constants(
		0, sizeof(float) * 4, frag_color,
		{bp::shader_stage::fragment}
	);
	draw->bind_vertex_buffers({{vbuffer, 0}});
	draw->draw(3, 1, 0, 0);

	uint32_t width = 640, height = 480, old_width = width, old_height = height;
	unsigned resize_delegate = bp::connect(
		events.resize,
		[&width, &height](uint32_t w, uint32_t h) {
			width = w;
			height = h;
		}
	);

	upload_job.wait();

	while (!glfwWindowShouldClose(window)) {
		batch->execute().wait();
		swapchain->swap();
		glfwWaitEvents();
		if (width != old_width || height != old_height) {
			framebuffer->resize(width, height);
			old_width = width;
			old_height = height;
			render_pass_instance->render_area(0, 0, width, height);
		}
	}

	events.resize.detach(resize_delegate);
}

void resize_callback(GLFWwindow* window, int w, int h) {
	window_events* events =
		reinterpret_cast<window_events*>(glfwGetWindowUserPointer(window));
	events->resize(static_cast<uint32_t>(w), static_cast<uint32_t>(h));
}

int main(int argc, char** argv) {
	if (!glfwInit()) {
		cerr << "Failed to initialize GLFW." << endl;
		return 1;
	}
	atexit(glfwTerminate);

	glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
	GLFWwindow* window = glfwCreateWindow(
		640,
		480,
		"bp example",
		nullptr,
		nullptr
	);
	if (!window) {
		cerr << "Failed to create window." << endl;
		return 1;
	}

	window_events events;
	glfwSetWindowUserPointer(window, &events);
	glfwSetWindowSizeCallback(window, resize_callback);

	int result = 0;
	try {
		run(bp::path(bp::base_path(argv[0]), "spv"), window, events);
	} catch (const exception& e) {
		cerr << e.what() << endl;
		result = 1;
	}

	glfwDestroyWindow(window);
	return result;
}