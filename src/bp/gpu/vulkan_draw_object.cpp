#define BP_API_IMPL
#include "vulkan_draw_object.hpp"
#include "vulkan_subpass_instance.hpp"
#include "vulkan_queue.hpp"
#include "vulkan_graphics_pipeline.hpp"
#include "vulkan_pipeline_layout.hpp"
#include "vulkan_buffer.hpp"
#include "vulkan_subpass_command.hpp"
#include "vulkan_enum_util.hpp"
#include "vulkan_command_recorder.hpp"
#include <cstring>

using namespace std;

namespace bp {

vulkan_draw_object::vulkan_draw_object(
	weak_ptr<vulkan_subpass_instance> subpass_instance,
	uint32_t frame_count
) : subpass_instance{subpass_instance} {
	cmd_buffers.resize(frame_count);
}

future<shared_ptr<vulkan_command_buffer>> vulkan_draw_object::prepare(
	shared_ptr<bp::vulkan_queue> queue,
	VkViewport viewport,
	VkRect2D scissor,
	const VkCommandBufferInheritanceInfo& inheritance_info,
	uint32_t frame_index
) {
	if (auto cmd_buffer = cmd_buffers[frame_index]; cmd_buffer) {
		return async(
			launch::deferred,
			[cmd_buffer] { return cmd_buffer; }
		);
	}

	auto fut = queue->record_commands(
		VK_COMMAND_BUFFER_LEVEL_SECONDARY,
		[this, viewport, scissor](VkCommandBuffer cmd_buf) {
			vulkan_command_recorder recorder{cmd_buf, viewport, scissor};
			for (auto& cmd : commands) {
				visit(recorder, cmd);
			}
		},
		VK_COMMAND_BUFFER_USAGE_RENDER_PASS_CONTINUE_BIT,
		&inheritance_info
	);

	return async(
		launch::deferred,
		[this, fut = move(fut), frame_index] () mutable {
			cmd_buffers[frame_index] = fut.get();
			return cmd_buffers[frame_index];
		}
	);
}

void vulkan_draw_object::bind_pipeline(
	shared_ptr<bp::pipeline> pipeline
) {
	commands.push_back(
		bind_pipeline_command{
			static_pointer_cast<vulkan_graphics_pipeline>(pipeline)
		}
	);
}

void vulkan_draw_object::push_constants(
	uint32_t offset,
	uint32_t size,
	const void* data,
	vector<shader_stage> shader_stages
) {
	vector<uint8_t> data_copy{
		static_cast<const uint8_t*>(data),
		static_cast<const uint8_t*>(data) + size
	};
	commands.push_back(
		push_constant_command{
			to_vulkan_shader_stage_mask(shader_stages),
			offset,
			move(data_copy)
		}
	);
}

void vulkan_draw_object::bind_vertex_buffers(
	vector<vertex_buffer_binding> bindings,
	uint32_t first_binding
) {
	vector<shared_ptr<vulkan_buffer>> buffers;
	vector<VkDeviceSize> offsets;

	for (auto& b : bindings) {
		buffers.push_back(
			static_pointer_cast<vulkan_buffer>(b.buffer)
		);
		offsets.push_back(b.offset);
	}

	commands.push_back(
		bind_vertex_buffers_command{
			move(buffers),
			move(offsets),
			first_binding
		}
	);
}

void vulkan_draw_object::bind_index_buffer(
	shared_ptr<bp::buffer> buffer,
	size_t offset,
	bp::index_type index_type
) {
	commands.push_back(
		bind_index_buffer_command{
			static_pointer_cast<vulkan_buffer>(buffer),
			offset,
			index_type
		}
	);
}

void vulkan_draw_object::draw(
	uint32_t vertex_count,
	uint32_t instance_count,
	uint32_t first_vertex,
	uint32_t first_instance
) {
	commands.push_back(
		draw_command{
			vertex_count,
			instance_count,
			first_vertex,
			first_instance
		}
	);
}

void vulkan_draw_object::draw_indexed(
	uint32_t index_count,
	uint32_t instance_count,
	uint32_t first_index,
	int32_t vertex_offset,
	uint32_t first_instance
) {
	commands.push_back(
		draw_indexed_command{
			index_count,
			instance_count,
			first_index,
			vertex_offset,
			first_instance
		}
	);
}

void vulkan_draw_object::dirty() {
	size_t count = cmd_buffers.size();
	cmd_buffers.clear();
	cmd_buffers.resize(count);
}

}
