#pragma once
#include <bp/gpu/shader.hpp>
#include <bp/gpu/gpu_error.hpp>
#include <vulkan/vulkan.h>
#include <vector>

namespace bp {

inline VkShaderStageFlagBits to_vulkan_shader_stage(shader_stage s) {
	switch (s) {
	case shader_stage::vertex:
		return VK_SHADER_STAGE_VERTEX_BIT;
	case shader_stage::tesselation_control:
		return VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT;
	case shader_stage::tesselation_evaluation:
		return VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT;
	case shader_stage::geometry:
		return VK_SHADER_STAGE_GEOMETRY_BIT;
	case shader_stage::fragment:
		return VK_SHADER_STAGE_FRAGMENT_BIT;
	case shader_stage::compute:
		return VK_SHADER_STAGE_COMPUTE_BIT;
	default:
		throw gpu_error{
			"Unknown shader stage"
		};
	}
}

inline VkShaderStageFlags to_vulkan_shader_stage_mask(
	const std::vector<shader_stage>& stages
) {
	VkShaderStageFlags flags = 0;
	for (auto s : stages) flags |= to_vulkan_shader_stage(s);
	return flags;
}

}