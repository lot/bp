#pragma once
#include "vulkan_descriptor_counts.hpp"
#include <vulkan/vulkan.h>
#include <memory>

#include <bp/util/api.hpp>

namespace bp {

class vulkan_context;

class BP_API vulkan_descriptor_pool {
	std::shared_ptr<vulkan_context> context;
	VkDescriptorPool m_handle;
	vulkan_descriptor_counts m_remaining_descriptors;

public:
	vulkan_descriptor_pool(
		std::shared_ptr<vulkan_context> context,
		uint32_t max_sets,
		const vulkan_descriptor_counts& pool_sizes
	);
	~vulkan_descriptor_pool();

	VkDescriptorPool handle() { return m_handle; }

	const vulkan_descriptor_counts& remaining_descriptors() const {
		return m_remaining_descriptors;
	}
};

}

#undef BP_API