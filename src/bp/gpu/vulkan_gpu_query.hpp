#pragma once
#include <bp/gpu/gpu_query.hpp>
#include "vulkan_gpu.hpp"
#include <vector>

#include <bp/util/api.hpp>

namespace bp {

class BP_API vulkan_gpu_query : public gpu_query {
	const std::vector<vulkan_gpu>& vulkan_gpus;

	gpu_usage queue_family_capability(
		const vulkan_gpu& gpu,
		uint32_t family
	);

public:
	vulkan_gpu_query(const std::vector<vulkan_gpu>& vulkan_gpus) :
		vulkan_gpus{vulkan_gpus} {}

	std::vector<gpu> results() override;
};

}

#undef BP_API