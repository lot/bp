#define BP_API_IMPL
#include "vulkan_buffer.hpp"
#include "vulkan_context.hpp"
#include <bp/gpu/gpu_error.hpp>

using namespace std;

namespace bp {

vulkan_buffer::vulkan_buffer(
	shared_ptr<vulkan_context> context,
	buffer_usage usage,
	size_t size,
	VmaMemoryUsage memory_usage
) : context{context},
	m_usage{usage},
	m_size{size},
	memory_usage{memory_usage},
	mapped{nullptr} {
	VkBufferCreateInfo info;
	info.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
	info.pNext = nullptr;
	info.flags = 0;
	info.size = m_size;

	info.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT
				 | VK_BUFFER_USAGE_TRANSFER_DST_BIT;

	if (usage & buffer_usage::uniform)
		info.usage |= VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT;
	if (usage & buffer_usage::storage)
		info.usage |= VK_BUFFER_USAGE_STORAGE_BUFFER_BIT;
	if (usage & buffer_usage::index)
		info.usage |= VK_BUFFER_USAGE_INDEX_BUFFER_BIT;
	if (usage & buffer_usage::vertex)
		info.usage |= VK_BUFFER_USAGE_VERTEX_BUFFER_BIT;

	info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
	info.queueFamilyIndexCount = 0;
	info.pQueueFamilyIndices = nullptr;

	VmaAllocationCreateInfo allocation_create_info;
	allocation_create_info.flags = 0;
	allocation_create_info.usage = memory_usage;
	allocation_create_info.requiredFlags = 0;
	allocation_create_info.preferredFlags = 0;
	allocation_create_info.memoryTypeBits = 0;
	allocation_create_info.pool = VK_NULL_HANDLE;
	allocation_create_info.pUserData = nullptr;

	VmaAllocationInfo allocation_info;
	VkResult result = vmaCreateBuffer(
		context->memory_allocator(),
		&info,
		&allocation_create_info,
		&m_handle,
		&memory_allocation,
		&allocation_info
	);

	if (result != VK_SUCCESS) {
		throw gpu_error{
			"Failed to create gpu buffer."
		};
	}

	vmaGetMemoryTypeProperties(
		context->memory_allocator(),
		allocation_info.memoryType,
		&memory_property_flags
	);
}

vulkan_buffer::~vulkan_buffer() {
	vkDestroyBuffer(context->device(), m_handle, nullptr);
	vmaFreeMemory(context->memory_allocator(), memory_allocation);
}

uint8_t* vulkan_buffer::map() {
	if (mapped) return mapped;

	if (memory_property_flags & VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT) {
		VkResult result = vmaMapMemory(
			context->memory_allocator(),
			memory_allocation,
			reinterpret_cast<void**>(&mapped)
		);

		if (result != VK_SUCCESS) {
			throw gpu_error{
				"Failed to map gpu memory."
			};
		}
	} else {
		if (!staging_buffer) {
			staging_buffer = make_unique<vulkan_buffer>(
				context,
				buffer_usage{},
				m_size,
				VMA_MEMORY_USAGE_CPU_ONLY
			);
		}
		mapped = staging_buffer->map();
	}

	return mapped;
}

void vulkan_buffer::unmap() {
	if (mapped == nullptr) return;

	if (staging_buffer) {
		staging_buffer->unmap();
	} else {
		vmaUnmapMemory(
			context->memory_allocator(),
			memory_allocation
		);
	}

	mapped = nullptr;
}

}