#define BP_API_IMPL
#include "vulkan_descriptor_pool.hpp"
#include "vulkan_context.hpp"
#include <bp/gpu/gpu_error.hpp>
#include <vector>

using namespace std;

namespace bp {

static vector<VkDescriptorPoolSize> to_pool_sizes(
	const vulkan_descriptor_counts& counts
) {
	vector<VkDescriptorPoolSize> pool_sizes;

	if (counts.sampler_count > 0) {
		pool_sizes.push_back(
			{
				VK_DESCRIPTOR_TYPE_SAMPLER,
				counts.sampler_count
			}
		);
	}
	if (counts.combined_image_sampler_count > 0) {
		pool_sizes.push_back(
			{
				VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
				counts.combined_image_sampler_count
			}
		);
	}
	if (counts.sampled_image_count > 0) {
		pool_sizes.push_back(
			{
				VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE,
				counts.sampled_image_count
			}
		);
	}
	if (counts.storage_image_count > 0) {
		pool_sizes.push_back(
			{
				VK_DESCRIPTOR_TYPE_STORAGE_IMAGE,
				counts.storage_image_count
			}
		);
	}
	if (counts.uniform_texel_buffer_count > 0) {
		pool_sizes.push_back(
			{
				VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER,
				counts.uniform_texel_buffer_count
			}
		);
	}
	if (counts.storage_texel_buffer_count > 0) {
		pool_sizes.push_back(
			{
				VK_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER,
				counts.storage_texel_buffer_count
			}
		);
	}
	if (counts.uniform_buffer_count > 0) {
		pool_sizes.push_back(
			{
				VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
				counts.uniform_buffer_count
			}
		);
	}
	if (counts.storage_buffer_count > 0) {
		pool_sizes.push_back(
			{
				VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
				counts.storage_buffer_count
			}
		);
	}
	if (counts.uniform_buffer_dynamic_count > 0) {
		pool_sizes.push_back(
			{
				VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC,
				counts.uniform_buffer_dynamic_count
			}
		);
	}
	if (counts.storage_buffer_dynamic_count > 0) {
		pool_sizes.push_back(
			{
				VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC,
				counts.storage_buffer_dynamic_count
			}
		);
	}

	return pool_sizes;
}

vulkan_descriptor_pool::vulkan_descriptor_pool(
	shared_ptr<bp::vulkan_context> context,
	uint32_t max_sets,
	const vulkan_descriptor_counts& pool_sizes
) : context{context},
	m_remaining_descriptors{pool_sizes} {
	auto vk_pool_sizes = to_pool_sizes(pool_sizes);

	VkDescriptorPoolCreateInfo info;
	info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
	info.pNext = nullptr;
	info.flags = 0;
	info.maxSets = max_sets;
	info.poolSizeCount = static_cast<uint32_t>(vk_pool_sizes.size());
	info.pPoolSizes = vk_pool_sizes.data();

	VkResult result = vkCreateDescriptorPool(
		context->device(),
		&info,
		nullptr,
		&m_handle
	);
	if (result != VK_SUCCESS) {
		throw gpu_error{"Failed to create descriptor pool."};
	}
}

vulkan_descriptor_pool::~vulkan_descriptor_pool() {
	vkDestroyDescriptorPool(context->device(), m_handle, nullptr);
}

}