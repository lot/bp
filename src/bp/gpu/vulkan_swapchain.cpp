#define BP_API_IMPL
#include "vulkan_swapchain.hpp"
#include "vulkan_context.hpp"
#include "vulkan_surface.hpp"
#include "vulkan_semaphore.hpp"
#include "vulkan_queue.hpp"
#include "vulkan_attachment.hpp"
#include <bp/gpu/gpu_error.hpp>

using namespace std;

namespace bp {

static void select_image_count(
	const VkSurfaceCapabilitiesKHR& capabilities,
	size_t& count
) {
	if (count < capabilities.minImageCount) {
		count = capabilities.minImageCount;
	} else if (capabilities.maxImageCount != 0
			   && count > capabilities.maxImageCount) {
		count = capabilities.maxImageCount;
	}
}

static void select_swapchain_size(
	const VkSurfaceCapabilitiesKHR& capabilities,
	size_t& width,
	size_t& height
) {
	if (capabilities.currentExtent.width == 0xFFFFFFFF) {
		if (width < capabilities.minImageExtent.width) {
			width = capabilities.minImageExtent.width;
		} else if (width > capabilities.maxImageExtent.width) {
			width = capabilities.maxImageExtent.width;
		}
		if (height < capabilities.minImageExtent.height) {
			height = capabilities.minImageExtent.height;
		} else if (height > capabilities.maxImageExtent.height) {
			height = capabilities.maxImageExtent.height;
		}
	} else {
		width = capabilities.currentExtent.width;
		height = capabilities.currentExtent.height;
	}
}

static void select_pre_transform(
	const VkSurfaceCapabilitiesKHR& capabilities,
	VkSurfaceTransformFlagBitsKHR& pre_transform
) {
	if (capabilities.supportedTransforms
		& VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR) {
		pre_transform = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
	} else {
		pre_transform = capabilities.currentTransform;
	}
}

static void select_surface_format(
	VkPhysicalDevice physical_device,
	VkSurfaceKHR surface,
	VkSurfaceFormatKHR& surface_format
) {
	uint32_t format_count;
	VkResult result = vkGetPhysicalDeviceSurfaceFormatsKHR(
		physical_device,
		surface,
		&format_count,
		nullptr
	);
	if (result != VK_SUCCESS || format_count == 0) {
		throw gpu_error{
			"Failed to enumerate surface formats."
		};
	}

	vector<VkSurfaceFormatKHR> formats{format_count};
	result = vkGetPhysicalDeviceSurfaceFormatsKHR(
		physical_device,
		surface,
		&format_count,
		formats.data()
	);
	if (result != VK_SUCCESS || format_count == 0) {
		throw gpu_error{
			"Failed to enumerate surface formats."
		};
	}

	if (format_count == 1 && formats[0].format == VK_FORMAT_UNDEFINED) {
		surface_format.format = VK_FORMAT_B8G8R8_UNORM;
		surface_format.colorSpace = VK_COLOR_SPACE_SRGB_NONLINEAR_KHR;
	} else {
		uint32_t i;
		for (i = 0; i < format_count; i++) {
			if (formats[i].format == VK_FORMAT_B8G8R8A8_UNORM) {
				surface_format = formats[i];
				break;
			}
		}
		if (i == format_count) {
			surface_format = formats[0];
		}
	}
}

static void select_present_mode(
	VkPhysicalDevice physical_device,
	VkSurfaceKHR surface,
	VkPresentModeKHR& present_mode
) {
	if (present_mode == VK_PRESENT_MODE_FIFO_KHR) return;
	present_mode = VK_PRESENT_MODE_FIFO_KHR;

	uint32_t present_mode_count;
	VkResult result = vkGetPhysicalDeviceSurfacePresentModesKHR(
		physical_device,
		surface,
		&present_mode_count,
		nullptr
	);
	if (result != VK_SUCCESS) {
		throw gpu_error{
			"Failed to enumerate present modes."
		};
	}
	if (present_mode_count == 0) return;

	vector<VkPresentModeKHR> present_modes{present_mode_count};
	result = vkGetPhysicalDeviceSurfacePresentModesKHR(
		physical_device,
		surface,
		&present_mode_count,
		present_modes.data()
	);
	if (result != VK_SUCCESS) {
		throw gpu_error{
			"Failed to enumerate present modes."
		};
	}

	for (uint32_t i = 0; i < present_mode_count; i++) {
		if (present_modes[i] == VK_PRESENT_MODE_MAILBOX_KHR) {
			present_mode = present_modes[i];
			break;
		} else if (present_modes[i] == VK_PRESENT_MODE_FIFO_RELAXED_KHR
				   || (present_modes[i] == VK_PRESENT_MODE_IMMEDIATE_KHR
					   && present_mode == VK_PRESENT_MODE_FIFO_KHR)) {
			present_mode = present_modes[i];
		}
	}
}

void vulkan_swapchain::create() {
	VkSurfaceCapabilitiesKHR surface_capabilities;
	VkResult result = vkGetPhysicalDeviceSurfaceCapabilitiesKHR(
		context->physical_device(),
		surface->handle(),
		&surface_capabilities
	);
	if (result != VK_SUCCESS) {
		throw gpu_error{
			"Failed to retrieve surface capabilities."
		};
	}

	select_image_count(surface_capabilities, image_count);
	select_swapchain_size(surface_capabilities, m_width, m_height);

	VkFormat old_format = m_format.format;
	select_surface_format(
		context->physical_device(),
		surface->handle(),
		m_format
	);
	if (handle != VK_NULL_HANDLE && m_format.format != old_format) {
		throw gpu_error{
			"Unexpected change of surface format"
		};
	}

	VkSwapchainCreateInfoKHR info;
	info.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
	info.pNext = nullptr;
	info.flags = 0;
	info.surface = surface->handle();
	info.minImageCount = static_cast<uint32_t>(image_count);
	info.imageFormat = m_format.format;
	info.imageColorSpace = m_format.colorSpace;
	info.imageExtent.width = static_cast<uint32_t>(m_width);
	info.imageExtent.height = static_cast<uint32_t>(m_height);
	info.imageArrayLayers = 1;
	info.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
	info.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
	info.queueFamilyIndexCount = 0;
	info.pQueueFamilyIndices = nullptr;

	select_pre_transform(surface_capabilities, info.preTransform);

	info.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;

	info.presentMode = m_flags & swapchain_flags::vsync
					   ? VK_PRESENT_MODE_FIFO_KHR
					   : VK_PRESENT_MODE_IMMEDIATE_KHR;
	select_present_mode(
		context->physical_device(),
		surface->handle(),
		info.presentMode
	);

	info.clipped = VK_TRUE;
	info.oldSwapchain = handle;

	result = vkCreateSwapchainKHR(
		context->device(),
		&info,
		nullptr,
		&handle
	);
	if (result != VK_SUCCESS) {
		throw gpu_error{
			"Failed to create swapchain."
		};
	}

	if (info.oldSwapchain == VK_NULL_HANDLE) {
		images.resize(image_count);
		image_layouts.resize(image_count);
		image_access_masks.resize(image_count);
		image_views.resize(image_count);
	} else {
		for (VkImageView view : image_views) {
			vkDestroyImageView(context->device(), view, nullptr);
		}
		vkDestroySwapchainKHR(context->device(), info.oldSwapchain, nullptr);
	}

	uint32_t n;
	result = vkGetSwapchainImagesKHR(
		context->device(),
		handle,
		&n,
		nullptr
	);
	if (result != VK_SUCCESS || n != image_count) {
		throw gpu_error{
			"Failed to retrieve swapchain images."
		};
	}

	result = vkGetSwapchainImagesKHR(
		context->device(),
		handle,
		&n,
		images.data()
	);
	if (result != VK_SUCCESS) {
		throw gpu_error{
			"Failed to retrieve swapchain images."
		};
	}

	VkImageViewCreateInfo image_view_info;
	image_view_info.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
	image_view_info.pNext = nullptr;
	image_view_info.flags = 0;
	image_view_info.viewType = VK_IMAGE_VIEW_TYPE_2D;
	image_view_info.format = m_format.format;
	image_view_info.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
	image_view_info.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
	image_view_info.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
	image_view_info.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
	image_view_info.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
	image_view_info.subresourceRange.baseMipLevel = 0;
	image_view_info.subresourceRange.levelCount = 1;
	image_view_info.subresourceRange.baseArrayLayer = 0;
	image_view_info.subresourceRange.layerCount = 1;

	for (auto i = 0; i < image_count; i++) {
		image_view_info.image = images[i];
		result = vkCreateImageView(
			context->device(),
			&image_view_info,
			nullptr,
			image_views.data() + i
		);
		if (result != VK_SUCCESS) {
			throw gpu_error{
				"Failed to create swapchain image view."
			};
		}
		image_layouts[i] = VK_IMAGE_LAYOUT_UNDEFINED;
		image_access_masks[i] = 0;
	}
}

VkImageMemoryBarrier vulkan_swapchain::current_image_transition(
	VkImageLayout dst_layout,
	VkAccessFlags dst_access
) {
	VkImageMemoryBarrier barrier;
	barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
	barrier.pNext = nullptr;
	barrier.srcAccessMask = image_access_masks[current_image_index];
	barrier.dstAccessMask = dst_access;
	barrier.oldLayout = image_layouts[current_image_index];
	barrier.newLayout = dst_layout;
	barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	barrier.image = images[current_image_index];
	barrier.subresourceRange = {VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1};

	image_layouts[current_image_index] = dst_layout;
	image_access_masks[current_image_index] = dst_access;

	return barrier;
}

vulkan_swapchain::vulkan_swapchain(
	shared_ptr<bp::vulkan_context> context,
	shared_ptr<bp::vulkan_surface> surface,
	bp::swapchain_flags flags,
	size_t width,
	size_t height
) : context{context},
	surface{surface},
	m_flags{flags},
	m_width{width},
	m_height{height},
	m_format{},
	image_count{2},
	handle{VK_NULL_HANDLE},
	current_image_index{0} {

	image_available_sem = context->create_semaphore();
	create();
}

vulkan_swapchain::~vulkan_swapchain() {
	for (VkImageView view : image_views) {
		vkDestroyImageView(context->device(), view, nullptr);
	}
	vkDestroySwapchainKHR(context->device(), handle, nullptr);
}

void vulkan_swapchain::resize(size_t width, size_t height) {
	m_width = width;
	m_height = height;
	create();
}

void vulkan_swapchain::next_image() {
	VkResult result = vkAcquireNextImageKHR(
		context->device(),
		handle,
		UINT64_MAX,
		image_available_sem->handle(),
		VK_NULL_HANDLE,
		&current_image_index
	);
	if (result == VK_ERROR_OUT_OF_DATE_KHR) {
		invalidated_event();
		create();
		vkAcquireNextImageKHR(
			context->device(),
			handle,
			UINT64_MAX,
			image_available_sem->handle(),
			VK_NULL_HANDLE,
			&current_image_index
		);
	}
}

void vulkan_swapchain::swap() {
	VkSemaphore wait_sem = present_wait_sem->handle();
	VkPresentInfoKHR present_info = {};
	present_info.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
	present_info.pNext = nullptr;
	present_info.waitSemaphoreCount = 1;
	present_info.pWaitSemaphores = &wait_sem;
	present_info.swapchainCount = 1;
	present_info.pSwapchains = &handle;
	present_info.pImageIndices = &current_image_index;
	present_info.pResults = nullptr;
	vkQueuePresentKHR(context->graphics_queue()->handle(), &present_info);
	swap_event();
}

optional<VkImageMemoryBarrier> vulkan_swapchain::before_transition(
	shared_ptr<vulkan_attachment> attachment
) {
	VkImageLayout layout = image_layouts[current_image_index];
	VkAccessFlags access_flags = image_access_masks[current_image_index];

	VkImageLayout dst_layout;
	VkAccessFlags dst_access;

	if (attachment->flags() & vulkan_attachment::color_attachment) {
		dst_layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
		dst_access = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT
					 | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
	} else if (attachment->flags() & vulkan_attachment::input_attachment) {
		dst_layout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		dst_access = VK_ACCESS_INPUT_ATTACHMENT_READ_BIT;
	} else {
		return nullopt;
	}

	if (dst_layout == layout || dst_access == access_flags) {
		return nullopt;
	}

	return optional<VkImageMemoryBarrier>{
		current_image_transition(dst_layout, dst_access)
	};
}

optional<VkImageMemoryBarrier> vulkan_swapchain::after_transition(
	shared_ptr<bp::vulkan_attachment> attachment
) {
	return optional<VkImageMemoryBarrier>{
		current_image_transition(
			VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
			VK_ACCESS_MEMORY_READ_BIT
		)
	};
}

}