#pragma once
#include <vulkan/vulkan.h>
#include <memory>

#include <bp/util/api.hpp>

namespace bp {

class vulkan_context;

class BP_API vulkan_semaphore {
	std::shared_ptr<vulkan_context> context;
	VkSemaphore m_handle;

public:
	vulkan_semaphore(std::shared_ptr<vulkan_context> context);

	~vulkan_semaphore();

	VkSemaphore handle() { return m_handle; }
};

}

#undef BP_API