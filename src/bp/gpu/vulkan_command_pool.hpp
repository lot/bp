#pragma once
#include <vulkan/vulkan.h>
#include <memory>
#include <vector>
#include <mutex>

#include <bp/util/api.hpp>

namespace bp {

class vulkan_command_buffer;

class BP_API vulkan_command_pool {
	VkDevice device;
	VkCommandPool handle;
	std::vector<VkCommandBuffer> remove_list;
	std::mutex m_mutex;

	void free(VkCommandBuffer cmd_buffer);
public:
	vulkan_command_pool(
		VkDevice device,
		uint32_t family_index
	);
	~vulkan_command_pool();

	std::shared_ptr<vulkan_command_buffer> allocate_command_buffer(
		VkCommandBufferLevel level
	);

	std::mutex& mutex() { return m_mutex; }
};

}

#undef BP_API