#define BP_API_IMPL
#include "vulkan_subpass_instance.hpp"
#include "vulkan_subpass.hpp"
#include "vulkan_queue.hpp"
#include "vulkan_render_pass.hpp"
#include "vulkan_render_pass_instance.hpp"
#include "vulkan_framebuffer.hpp"
#include "vulkan_graphics_pipeline.hpp"
#include "vulkan_pipeline_layout.hpp"
#include "vulkan_buffer.hpp"
#include "vulkan_subpass_command.hpp"
#include "vulkan_enum_util.hpp"
#include "vulkan_draw_object.hpp"
#include "vulkan_command_buffer.hpp"
#include <bp/gpu/gpu_error.hpp>
#include <cstring>

using namespace std;

namespace bp {

vulkan_subpass_instance::vulkan_subpass_instance(
	shared_ptr<bp::vulkan_render_pass_instance> render_pass_instance,
	shared_ptr<bp::vulkan_subpass> subpass,
	uint32_t frame_count
) : render_pass_instance{render_pass_instance},
	subpass{subpass},
	frame_count{frame_count} {
	dirty_delegate = connect(
		render_pass_instance->dirty_event,
		*this, &vulkan_subpass_instance::dirty
	);
	inheritance_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_INFO;
	inheritance_info.pNext = nullptr;
	inheritance_info.occlusionQueryEnable = VK_FALSE;
	inheritance_info.queryFlags = 0;
	inheritance_info.pipelineStatistics = 0;
}

vulkan_subpass_instance::~vulkan_subpass_instance() {
	auto rpi = render_pass_instance.lock();
	if (rpi) {
		rpi->dirty_event.detach(dirty_delegate);
	}
}

future<vector<shared_ptr<vulkan_command_buffer>>>
vulkan_subpass_instance::prepare(
	shared_ptr<bp::vulkan_queue> queue,
	const VkViewport& viewport,
	const VkRect2D& scissor,
	uint32_t frame_index
) {
	auto render_pass = subpass->render_pass.lock();
	auto instance = render_pass_instance.lock();
	inheritance_info.renderPass = render_pass->handle();
	inheritance_info.subpass = subpass->index;
	inheritance_info.framebuffer = instance->framebuffer()->current_handle();

	vector<future<shared_ptr<vulkan_command_buffer>>> record_jobs;
	for (auto& o : draw_objects) {
		record_jobs.push_back(
			o->prepare(queue, viewport, scissor, inheritance_info, frame_index)
		);
	}

	return async(
		launch::deferred,
		[
			this,
			record_jobs = move(record_jobs)
		] () mutable {
			vector<shared_ptr<vulkan_command_buffer>> cmd_bufs;
			for (auto& j : record_jobs) {
				cmd_bufs.push_back(j.get());
			}
			return cmd_bufs;
		}
	);
}

void vulkan_subpass_instance::dirty() {
	for (auto& d : draw_objects) {
		d->dirty();
	}
}

shared_ptr<draw_object> vulkan_subpass_instance::add_draw_object() {
	draw_objects.push_back(
		make_shared<vulkan_draw_object>(shared_from_this(), frame_count)
	);
	return draw_objects.back();
}

}