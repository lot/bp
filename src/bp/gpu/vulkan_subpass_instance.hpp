#pragma once
#include <bp/gpu/subpass_instance.hpp>
#include "vulkan_subpass_command.hpp"
#include <memory>
#include <future>
#include <vector>

#include <bp/util/api.hpp>

namespace bp {

class vulkan_render_pass_instance;
class vulkan_subpass;
class vulkan_command_buffer;
class vulkan_queue;
class vulkan_draw_object;

class BP_API vulkan_subpass_instance :
	public subpass_instance,
	public std::enable_shared_from_this<vulkan_subpass_instance> {
	friend class vulkan_render_pass_instance;
	friend class vulkan_draw_object;
	std::weak_ptr<vulkan_render_pass_instance> render_pass_instance;
	unsigned dirty_delegate;
	std::shared_ptr<vulkan_subpass> subpass;
	uint32_t frame_count;
	VkCommandBufferInheritanceInfo inheritance_info;

	std::vector<std::shared_ptr<vulkan_draw_object>> draw_objects;
public:
	vulkan_subpass_instance(
		std::shared_ptr<vulkan_render_pass_instance> render_pass_instance,
		std::shared_ptr<vulkan_subpass> subpass,
		uint32_t frame_count
	);
	~vulkan_subpass_instance();

	std::future<std::vector<std::shared_ptr<vulkan_command_buffer>>> prepare(
		std::shared_ptr<vulkan_queue> queue,
		const VkViewport& viewport,
		const VkRect2D& scissor,
		uint32_t frame_index
	);

	void dirty();

	std::shared_ptr<draw_object> add_draw_object() override;
};

}

#undef BP_API