#define BP_API_IMPL
#include "vulkan_gpu_query.hpp"
#include "vulkan_surface.hpp"

using namespace std;

namespace bp {

gpu_usage vulkan_gpu_query::queue_family_capability(
	const vulkan_gpu& vkgpu,
	uint32_t family
) {
	gpu_usage capability = 0;

	uint32_t queue_flags = vkgpu.queue_families[family].queueFlags;
	if (queue_flags & VK_QUEUE_GRAPHICS_BIT) {
		VkBool32 supported = VK_TRUE;

		gpu_usage gfx_flags = gpu_usage::graphics;
		if (usage() & gpu_usage::surface) {
			gfx_flags |= gpu_usage::surface;
			VkSurfaceKHR surf =
				static_pointer_cast<vulkan_surface>(surface())
					->handle();
			vkGetPhysicalDeviceSurfaceSupportKHR(
				vkgpu.physical_device, family, surf,
				&supported
			);
		}

		if (supported)
			capability |= gfx_flags;
	}
	if (queue_flags & VK_QUEUE_COMPUTE_BIT)
		capability |= gpu_usage::compute;

	return capability;
}

vector<gpu> vulkan_gpu_query::results() {
	vector<gpu> suitable_gpus;

	for (unsigned gpu_id = 0; gpu_id < vulkan_gpus.size(); gpu_id++) {
		const vulkan_gpu& vkgpu = vulkan_gpus[gpu_id];
		gpu_usage capability = 0;

		for (unsigned qfi = 0; qfi < vkgpu.queue_families.size(); qfi++) {
			capability |= queue_family_capability(vkgpu, qfi);
			if ((capability & usage()) == usage())
				break;
		}
		if ((capability & usage()) == usage()) {
			suitable_gpus.emplace_back(
				gpu_id, string{vkgpu.properties.deviceName}
			);
		}
	}

	return suitable_gpus;
}

}