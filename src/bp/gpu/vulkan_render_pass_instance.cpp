#define BP_API_IMPL
#include "vulkan_render_pass_instance.hpp"
#include "vulkan_render_pass.hpp"
#include "vulkan_framebuffer.hpp"
#include "vulkan_subpass_instance.hpp"
#include "vulkan_subpass.hpp"
#include "vulkan_queue.hpp"
#include "vulkan_command_buffer.hpp"
#include "vulkan_context.hpp"
#include "vulkan_swapchain.hpp"

using namespace std;

namespace bp {

vulkan_render_pass_instance::vulkan_render_pass_instance(
	shared_ptr<bp::vulkan_batch> batch,
	shared_ptr<bp::vulkan_render_pass> render_pass,
	shared_ptr<bp::vulkan_framebuffer> framebuffer,
	int32_t x_pos,
	int32_t y_pos,
	uint32_t width,
	uint32_t height
) : batch{batch},
	render_pass{render_pass},
	m_framebuffer{framebuffer},
	m_render_area{{x_pos, y_pos}, {width, height}} {
	if (!render_pass->ready()) {
		render_pass->init();
	}
	if (!m_framebuffer->ready()) {
		m_framebuffer->init();
	}
	frame_count = m_framebuffer->frame_count();
	cmd_buffers.resize(frame_count);
	if (framebuffer->swapchain()) {
		swap_delegate = connect(
			framebuffer->swapchain()->swap_event,
			[this] {
				swapchain_next = true;
				dirty_event();
			}
		);
		swapchain_invalidated_event = connect(
			framebuffer->swapchain()->invalidated_event,
			[this] {
				cmd_buffers.clear();
				cmd_buffers.resize(frame_count);
				swapchain_next = true;
				dirty_event();
			}
		);
	}
	resize_delegate = connect(
		framebuffer->resize_event,
		[this](size_t, size_t) {
			cmd_buffers.clear();
			cmd_buffers.resize(frame_count);
			swapchain_next = true;
			dirty_event();
		}
	);
}

vulkan_render_pass_instance::~vulkan_render_pass_instance() {
	if (framebuffer()->swapchain()) {
		framebuffer()->swapchain()->swap_event.detach(swap_delegate);
		framebuffer()->swapchain()->invalidated_event.detach(
			swapchain_invalidated_event
		);
	}
	framebuffer()->resize_event.detach(resize_delegate);
}

shared_ptr<subpass_instance> vulkan_render_pass_instance::subpass(
	shared_ptr<bp::subpass> pass
) {
	auto subpass_instance = make_shared<vulkan_subpass_instance>(
		shared_from_this(),
		static_pointer_cast<vulkan_subpass>(pass),
		frame_count
	);
	subpass_instances.push_back(subpass_instance);
	return subpass_instance;
}

void vulkan_render_pass_instance::render_area(
	int32_t x_pos,
	int32_t y_pos,
	uint32_t width,
	uint32_t height
) {
	m_render_area = {{x_pos, y_pos}, {width, height}};
}

future<shared_ptr<vulkan_command_buffer>> vulkan_render_pass_instance::prepare(
	shared_ptr<bp::vulkan_queue> queue,
	function<
	    void(shared_ptr<vulkan_semaphore>, VkPipelineStageFlags)
	> wait_semaphore,
	function<void(shared_ptr<vulkan_semaphore>)> signal_semaphore
) {
	if (m_framebuffer->swapchain() && !render_complete_sem) {
		render_complete_sem = render_pass->context->create_semaphore();
		m_framebuffer->swapchain()->present_wait_semaphore(render_complete_sem);
	}

	if (m_framebuffer->swapchain()) {
		if (swapchain_next) {
			m_framebuffer->swapchain()->next_image();
			swapchain_next = false;
		}
		wait_semaphore(
			m_framebuffer->swapchain()->image_available_semaphore(),
			VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT
		);
		signal_semaphore(render_complete_sem);
	}

	uint32_t frame_index = m_framebuffer->current_frame_index();
	if (auto cmd_buffer = cmd_buffers[frame_index]; cmd_buffer) {
		return async(
			launch::deferred,
			[cmd_buffer] { return cmd_buffer; }
		);
	}

	return async(
		launch::async,
		[
			this,
			queue,
			frame_index
		] {
			VkViewport viewport = {
				static_cast<float>(m_render_area.offset.x),
				static_cast<float>(m_render_area.offset.y),
				static_cast<float>(m_render_area.extent.width),
				static_cast<float>(m_render_area.extent.height),
				0.f, 1.f
			};

			vector<future<vector<shared_ptr<vulkan_command_buffer>>>>
				subpass_jobs;
			subpass_jobs.reserve(subpass_instances.size());
			for (auto& s : subpass_instances) {
				subpass_jobs.push_back(
					s->prepare(
						queue,
						viewport,
						m_render_area,
						frame_index
					)
				);
			}

			vector<vector<shared_ptr<vulkan_command_buffer>>>
			    subpass_cmd_buffers;
			subpass_cmd_buffers.reserve(subpass_instances.size());
			for (unsigned i = 0; i < subpass_instances.size(); i++) {
				subpass_cmd_buffers.push_back(subpass_jobs[i].get());
			}
			subpass_jobs.clear();

			auto cmd_buf_fut = queue->record_commands(
				VK_COMMAND_BUFFER_LEVEL_PRIMARY,
				[
					this,
					subpasses = move(subpass_cmd_buffers)
				](
					VkCommandBuffer cmd_buf
				) {
					m_framebuffer->before_transitions(cmd_buf);

					VkRenderPassBeginInfo begin_info;
					begin_info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
					begin_info.pNext = nullptr;
					begin_info.renderPass = render_pass->handle();
					begin_info.framebuffer = m_framebuffer->current_handle();
					begin_info.renderArea = m_render_area;

					auto clear_values = m_framebuffer->clear_values();
					begin_info.clearValueCount =
						static_cast<uint32_t>(clear_values.size());
					begin_info.pClearValues = clear_values.data();

					vkCmdBeginRenderPass(
						cmd_buf,
						&begin_info,
						VK_SUBPASS_CONTENTS_SECONDARY_COMMAND_BUFFERS
					);

					vector<VkCommandBuffer> cmd_bufs;
					for (auto& c : subpasses[0]) {
						cmd_bufs.push_back(c->handle());
					}

					vkCmdExecuteCommands(
						cmd_buf,
						static_cast<uint32_t>(cmd_bufs.size()),
						cmd_bufs.data()
					);

					for (unsigned i = 1; i < subpasses.size(); i++) {
						vkCmdNextSubpass(
							cmd_buf,
							VK_SUBPASS_CONTENTS_SECONDARY_COMMAND_BUFFERS
						);

						cmd_bufs.clear();
						for (auto& c : subpasses[i]) {
							cmd_bufs.push_back(c->handle());
						}

						vkCmdExecuteCommands(
							cmd_buf,
							static_cast<uint32_t>(cmd_bufs.size()),
							cmd_bufs.data()
						);
					}

					vkCmdEndRenderPass(cmd_buf);

					m_framebuffer->after_transitions(cmd_buf);
				}
			);

			cmd_buffers[frame_index] = cmd_buf_fut.get();
			return cmd_buffers[frame_index];
		}
	);
}

shared_ptr<vulkan_swapchain> vulkan_render_pass_instance::swapchain() {
	return m_framebuffer->swapchain();
}

}