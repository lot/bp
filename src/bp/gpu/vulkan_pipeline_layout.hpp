#pragma once
#include <bp/gpu/pipeline_layout.hpp>
#include <vulkan/vulkan.h>
#include <memory>
#include <vector>

#include <bp/util/api.hpp>

namespace bp {

class vulkan_context;

class BP_API vulkan_pipeline_layout : public pipeline_layout {
	std::shared_ptr<vulkan_context> context;
	std::vector<VkPushConstantRange> push_constant_ranges;
	std::vector<VkDescriptorSetLayoutBinding> m_descriptor_bindings;
	VkDescriptorSetLayout m_set_layout;
	VkPipelineLayout m_handle;

public:
	vulkan_pipeline_layout(std::shared_ptr<vulkan_context> context) :
		context{context},
		m_set_layout{VK_NULL_HANDLE},
		m_handle{VK_NULL_HANDLE} {}
	~vulkan_pipeline_layout();

	void init();

	VkPipelineLayout handle() { return m_handle; }

	VkDescriptorSetLayout set_layout() { return m_set_layout; }

	const std::vector<VkDescriptorSetLayoutBinding>&
	descriptor_bindings() const {
		return m_descriptor_bindings;
	}

	void add_push_constant_range(
		std::vector<shader_stage> shader_stages,
		uint32_t offset,
		uint32_t size
	) override;

	void add_descriptor_binding(
		uint32_t binding,
		std::vector<shader_stage> shader_stages,
		descriptor_type type,
		uint32_t count
	) override;
};

}

#undef BP_API