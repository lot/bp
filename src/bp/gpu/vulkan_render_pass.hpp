#pragma once
#include <bp/gpu/render_pass.hpp>
#include <vector>
#include <utility>
#include <vulkan/vulkan.h>

#include <bp/util/api.hpp>

namespace bp {

class vulkan_context;
class vulkan_attachment;
class vulkan_subpass;
class vulkan_framebuffer;
class vulkan_render_pass_instance;

class BP_API vulkan_render_pass :
	public render_pass,
	public std::enable_shared_from_this<vulkan_render_pass> {
	friend class vulkan_attachment;
	friend class vulkan_subpass;
	friend class vulkan_framebuffer;
	friend class vulkan_render_pass_instance;

	std::shared_ptr<vulkan_context> context;
	std::vector<std::shared_ptr<vulkan_attachment>> attachments;
	std::vector<std::shared_ptr<vulkan_subpass>> subpasses;
	VkRenderPass m_handle{VK_NULL_HANDLE};

	void order_subpasses();

	static bool resolve_dependency_src_access(
		std::shared_ptr<vulkan_subpass> src,
		std::shared_ptr<vulkan_attachment> attachment,
		VkAccessFlags& access
	);

	static std::pair<VkAccessFlags, VkAccessFlags> resolve_dependency_access(
		std::shared_ptr<vulkan_subpass> src,
		std::shared_ptr<vulkan_subpass> dst
	);

public:
	vulkan_render_pass(std::shared_ptr<vulkan_context> context);
	~vulkan_render_pass();

	std::shared_ptr<attachment> add_attachment(
		texture_format format,
		attachment_flags flags
	) override;

	std::shared_ptr<attachment> add_attachment(
		std::shared_ptr<bp::swapchain> swapchain,
		attachment_flags flags
	) override;

	std::shared_ptr<subpass> add_subpass() override;

	void init();

	bool ready() const { return m_handle != VK_NULL_HANDLE; }

	VkRenderPass handle() { return m_handle; }
};

}

#undef BP_API