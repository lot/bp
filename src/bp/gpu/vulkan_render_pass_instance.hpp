#pragma once
#include <bp/util/event.hpp>
#include <bp/gpu/render_pass_instance.hpp>
#include <memory>
#include <vector>
#include <functional>
#include <future>
#include <vulkan/vulkan.h>

#include <bp/util/api.hpp>

namespace bp {

class vulkan_batch;
class vulkan_render_pass;
class vulkan_framebuffer;
class vulkan_subpass_instance;
class vulkan_command_buffer;
class vulkan_queue;
class vulkan_semaphore;
class vulkan_swapchain;

class BP_API vulkan_render_pass_instance :
	public render_pass_instance,
	public std::enable_shared_from_this<vulkan_render_pass_instance> {
	std::weak_ptr<vulkan_batch> batch;
	std::shared_ptr<vulkan_render_pass> render_pass;
	std::shared_ptr<vulkan_framebuffer> m_framebuffer;
	uint32_t frame_count;
	std::vector<std::shared_ptr<vulkan_subpass_instance>> subpass_instances;
	std::vector<std::shared_ptr<vulkan_command_buffer>> cmd_buffers;
	std::shared_ptr<vulkan_semaphore> render_complete_sem;
	bool swapchain_next{true};
	unsigned swap_delegate;
	unsigned swapchain_invalidated_event;
	unsigned resize_delegate;
	VkRect2D m_render_area;

public:
	vulkan_render_pass_instance(
		std::shared_ptr<vulkan_batch> batch,
		std::shared_ptr<vulkan_render_pass> render_pass,
		std::shared_ptr<vulkan_framebuffer> framebuffer,
		int32_t x_pos,
		int32_t y_pos,
		uint32_t width,
		uint32_t height
	);
	~vulkan_render_pass_instance();

	std::shared_ptr<subpass_instance> subpass(
		std::shared_ptr<bp::subpass> pass
	) override;

	void render_area(
		int32_t x_pos,
		int32_t y_pos,
		uint32_t width,
		uint32_t height
	) override;

	std::future<std::shared_ptr<vulkan_command_buffer>> prepare(
		std::shared_ptr<vulkan_queue> queue,
		std::function<
		    void(std::shared_ptr<vulkan_semaphore>, VkPipelineStageFlags)
		> wait_semaphore,
		std::function<void(std::shared_ptr<vulkan_semaphore>)> signal_semaphore
	);

	std::shared_ptr<vulkan_swapchain> swapchain();

	std::shared_ptr<vulkan_framebuffer> framebuffer() { return m_framebuffer; }

	event<> dirty_event;
};

}

#undef BP_API