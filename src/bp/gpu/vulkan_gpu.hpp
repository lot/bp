#pragma once
#include <vulkan/vulkan.h>
#include <vector>

#include <bp/util/api.hpp>

namespace bp {

struct BP_API vulkan_gpu {
	VkPhysicalDevice physical_device;
	VkPhysicalDeviceProperties properties;
	VkPhysicalDeviceMemoryProperties memory_properties;
	std::vector<VkQueueFamilyProperties> queue_families;
	std::vector<VkExtensionProperties> extensions;
};

}

#undef BP_API