#pragma once
#include "vulkan_instance.hpp"
#include <bp/gpu/surface.hpp>
#include <vulkan/vulkan.h>

#include <bp/util/api.hpp>

namespace bp {

class BP_API vulkan_surface : public surface {
	std::shared_ptr<vulkan_instance> instance;
	VkSurfaceKHR m_handle;

public:
	vulkan_surface(
		std::shared_ptr<vulkan_instance> instance,
		VkSurfaceKHR handle
	);

	VkSurfaceKHR handle() const { return m_handle; }
};

}

#undef BP_API