#define BP_API_IMPL
#include "vulkan_graphics_pipeline.hpp"
#include "vulkan_context.hpp"
#include "vulkan_subpass.hpp"
#include "vulkan_shader.hpp"
#include "vulkan_pipeline_layout.hpp"
#include "vulkan_render_pass.hpp"
#include <bp/gpu/gpu_error.hpp>

using namespace std;

namespace bp {

static VkPrimitiveTopology vk_topology(primitive_topology t) {
	switch (t) {
	case primitive_topology::point_list:
		return VK_PRIMITIVE_TOPOLOGY_POINT_LIST;
	case primitive_topology::line_list:
		return VK_PRIMITIVE_TOPOLOGY_LINE_LIST;
	case primitive_topology::line_strip:
		return VK_PRIMITIVE_TOPOLOGY_LINE_STRIP;
	case primitive_topology::triangle_list:
		return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
	case primitive_topology::triangle_strip:
		return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP;
	case primitive_topology::triangle_fan:
		return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_FAN;
	case primitive_topology::line_list_with_adjacency:
		return VK_PRIMITIVE_TOPOLOGY_LINE_LIST_WITH_ADJACENCY;
	case primitive_topology::line_strip_with_adjacency:
		return VK_PRIMITIVE_TOPOLOGY_LINE_STRIP_WITH_ADJACENCY;
	case primitive_topology::triangle_list_with_adjacency:
		return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST_WITH_ADJACENCY;
	case primitive_topology::triangle_strip_with_adjacency:
		return VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP_WITH_ADJACENCY;
	case primitive_topology::patch_list:
		return VK_PRIMITIVE_TOPOLOGY_PATCH_LIST;
	default:
		throw gpu_error{
			"Unknown primitive topology."
		};
	}
}

VkPolygonMode vk_polygon_mode(polygon_mode mode) {
	switch (mode) {
	case polygon_mode::fill:
		return VK_POLYGON_MODE_FILL;
	case polygon_mode::line:
		return VK_POLYGON_MODE_LINE;
	case polygon_mode::point:
		return VK_POLYGON_MODE_POINT;
	default:
		throw gpu_error{
			"Unknown polygon mode."
		};
	}
}

vulkan_graphics_pipeline::vulkan_graphics_pipeline(
	std::shared_ptr<vulkan_context> context,
	std::shared_ptr<vulkan_pipeline_layout> layout,
	std::vector<std::shared_ptr<vulkan_shader>> shaders,
	std::shared_ptr<vulkan_subpass> subpass,
	std::vector<vertex_input_binding> vertex_bindings,
	std::vector<vertex_input_attribute> vertex_attributes,
	primitive_topology topology,
	bp::polygon_mode polygon_mode,
	graphics_pipeline_flags flags
) : context{context},
	m_layout{layout},
	shaders{move(shaders)},
	subpass{subpass},
	topology{vk_topology(topology)},
	polygon_mode{vk_polygon_mode(polygon_mode)},
	flags{flags},
	m_handle{VK_NULL_HANDLE} {
	vk_vertex_bindings.reserve(vertex_bindings.size());
	for (const auto& b : vertex_bindings) {
		vk_vertex_bindings.emplace_back();
		auto& vk_b = vk_vertex_bindings.back();
		vk_b.binding = b.binding;
		vk_b.stride = b.stride;
		vk_b.inputRate = b.input_rate == input_rate::instance
						 ? VK_VERTEX_INPUT_RATE_INSTANCE
						 : VK_VERTEX_INPUT_RATE_VERTEX;
	}

	vk_vertex_attributes.reserve(vertex_bindings.size());
	for (const auto& a : vertex_attributes) {
		vk_vertex_attributes.emplace_back();
		auto& vk_a = vk_vertex_attributes.back();
		vk_a.location = a.location;
		vk_a.binding = a.binding;

		switch (a.type) {
		case shader_data_type::b:
			vk_a.format = VK_FORMAT_R8_UINT;
			break;
		case shader_data_type::bvec2:
			vk_a.format = VK_FORMAT_R8G8_UINT;
			break;
		case shader_data_type::bvec3:
			vk_a.format = VK_FORMAT_R8G8B8_UINT;
			break;
		case shader_data_type::bvec4:
			vk_a.format = VK_FORMAT_R8G8B8A8_UINT;
			break;
		case shader_data_type::i:
			vk_a.format = VK_FORMAT_R32_SINT;
			break;
		case shader_data_type::ivec2:
			vk_a.format = VK_FORMAT_R32G32_SINT;
			break;
		case shader_data_type::ivec3:
			vk_a.format = VK_FORMAT_R32G32B32_SINT;
			break;
		case shader_data_type::ivec4:
			vk_a.format = VK_FORMAT_R32G32B32A32_SINT;
			break;
		case shader_data_type::f:
			vk_a.format = VK_FORMAT_R32_SFLOAT;
			break;
		case shader_data_type::vec2:
			vk_a.format = VK_FORMAT_R32G32_SFLOAT;
			break;
		case shader_data_type::vec3:
			vk_a.format = VK_FORMAT_R32G32B32_SFLOAT;
			break;
		case shader_data_type::vec4:
			vk_a.format = VK_FORMAT_R32G32B32A32_SFLOAT;
			break;
		default:
			throw gpu_error{
				"Unsupported vertex attribute type."
			};
		}

		vk_a.offset = a.offset;
	}
}

vulkan_graphics_pipeline::~vulkan_graphics_pipeline() {
	if (m_handle != VK_NULL_HANDLE) {
		vkDestroyPipeline(context->device(), m_handle, nullptr);
	}
}

void vulkan_graphics_pipeline::init() {
	if (m_layout->handle() == VK_NULL_HANDLE) {
		m_layout->init();
	}

	VkPipelineVertexInputStateCreateInfo vertex_input_state = {};
	vertex_input_state.sType =
		VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
	vertex_input_state.vertexBindingDescriptionCount =
		 static_cast<uint32_t>(vk_vertex_bindings.size());
	vertex_input_state.pVertexBindingDescriptions = vk_vertex_bindings.data();
	vertex_input_state.vertexAttributeDescriptionCount =
		static_cast<uint32_t>(vk_vertex_attributes.size());
	vertex_input_state.pVertexAttributeDescriptions =
		vk_vertex_attributes.data();


	VkPipelineInputAssemblyStateCreateInfo input_assembly_state = {};
	input_assembly_state.sType =
		VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
	input_assembly_state.topology = topology;
	input_assembly_state.primitiveRestartEnable =
		flags & graphics_pipeline_flags::primitive_restart;

	VkViewport vp = {};
	VkRect2D sc = {};

	VkPipelineViewportStateCreateInfo viewport_state = {};
	viewport_state.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
	viewport_state.viewportCount = 1;
	viewport_state.pViewports = &vp;
	viewport_state.scissorCount = 1;
	viewport_state.pScissors = &sc;

	VkPipelineRasterizationStateCreateInfo rasterization_state = {};
	rasterization_state.sType =
		VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
	rasterization_state.depthClampEnable = VK_FALSE;
	rasterization_state.rasterizerDiscardEnable = VK_FALSE;
	rasterization_state.polygonMode = polygon_mode;

	rasterization_state.cullMode = VK_CULL_MODE_NONE;
	if (flags & graphics_pipeline_flags::cull_front_face) {
		rasterization_state.cullMode |= VK_CULL_MODE_FRONT_BIT;
	}
	if (flags & graphics_pipeline_flags::cull_back_face) {
		rasterization_state.cullMode |= VK_CULL_MODE_BACK_BIT;
	}

	rasterization_state.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
	rasterization_state.depthBiasEnable = VK_FALSE;
	rasterization_state.depthBiasConstantFactor = 0;
	rasterization_state.depthBiasClamp = 0;
	rasterization_state.depthBiasSlopeFactor = 0;
	rasterization_state.lineWidth = 1;

	//TODO support MSAA
	VkPipelineMultisampleStateCreateInfo multisample_state = {};
	multisample_state.sType =
		VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
	multisample_state.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
	multisample_state.sampleShadingEnable = VK_FALSE;
	multisample_state.minSampleShading = 0;
	multisample_state.pSampleMask = nullptr;
	multisample_state.alphaToCoverageEnable = VK_FALSE;
	multisample_state.alphaToOneEnable = VK_FALSE;

	//TODO support stencil
	VkStencilOpState stencil_state = {};
	stencil_state.failOp = VK_STENCIL_OP_KEEP;
	stencil_state.passOp = VK_STENCIL_OP_KEEP;
	stencil_state.depthFailOp = VK_STENCIL_OP_KEEP;
	stencil_state.compareOp = VK_COMPARE_OP_ALWAYS;
	stencil_state.compareMask = 0;
	stencil_state.writeMask = 0;
	stencil_state.reference = 0;

	VkPipelineDepthStencilStateCreateInfo depth_state = {};
	depth_state.sType =
		VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;

	if (flags & graphics_pipeline_flags::depth_test) {
		depth_state.depthTestEnable = VK_TRUE;
		depth_state.depthWriteEnable = VK_TRUE;
	}

	depth_state.depthCompareOp = VK_COMPARE_OP_LESS_OR_EQUAL;
	depth_state.depthBoundsTestEnable = VK_FALSE;
	depth_state.stencilTestEnable = VK_FALSE;
	depth_state.front = stencil_state;
	depth_state.back = stencil_state;
	depth_state.minDepthBounds = 0;
	depth_state.maxDepthBounds = 0;

	//TODO support blend
	VkPipelineColorBlendAttachmentState blend_attachment_state = {};
	blend_attachment_state.blendEnable = VK_FALSE;
	blend_attachment_state.srcColorBlendFactor = VK_BLEND_FACTOR_SRC_COLOR;
	blend_attachment_state.dstColorBlendFactor =
		VK_BLEND_FACTOR_ONE_MINUS_DST_COLOR;
	blend_attachment_state.colorBlendOp = VK_BLEND_OP_ADD;
	blend_attachment_state.srcAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
	blend_attachment_state.dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
	blend_attachment_state.alphaBlendOp = VK_BLEND_OP_ADD;
	blend_attachment_state.colorWriteMask = 0xf;

	VkPipelineColorBlendStateCreateInfo color_blend_state = {};
	color_blend_state.sType =
		VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
	color_blend_state.logicOpEnable = VK_FALSE;
	color_blend_state.logicOp = VK_LOGIC_OP_CLEAR;
	color_blend_state.attachmentCount = 1;
	color_blend_state.pAttachments = &blend_attachment_state;
	color_blend_state.blendConstants[0] = 0.0;
	color_blend_state.blendConstants[1] = 0.0;
	color_blend_state.blendConstants[2] = 0.0;
	color_blend_state.blendConstants[3] = 0.0;

	VkDynamicState dynamic_state[2] = {
		VK_DYNAMIC_STATE_VIEWPORT,
		VK_DYNAMIC_STATE_SCISSOR
	};
	VkPipelineDynamicStateCreateInfo dynamic_state_info = {};
	dynamic_state_info.sType =
		VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
	dynamic_state_info.dynamicStateCount = 2;
	dynamic_state_info.pDynamicStates = dynamic_state;

	vector<VkPipelineShaderStageCreateInfo> shader_stages;
	for (auto& s : shaders) {
		shader_stages.push_back(s->stage_info());
	}

	VkGraphicsPipelineCreateInfo info = {};
	info.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
	info.stageCount = static_cast<uint32_t>(shader_stages.size());
	info.pStages = shader_stages.data();
	info.pVertexInputState = &vertex_input_state;
	info.pInputAssemblyState = &input_assembly_state;
	info.pTessellationState = nullptr;
	info.pViewportState = &viewport_state;
	info.pRasterizationState = &rasterization_state;
	info.pMultisampleState = &multisample_state;
	info.pDepthStencilState = &depth_state;
	info.pColorBlendState = &color_blend_state;
	info.pDynamicState = &dynamic_state_info;
	info.layout = m_layout->handle();
	info.renderPass = subpass->render_pass.lock()->handle();
	info.subpass = 0;
	info.basePipelineHandle = VK_NULL_HANDLE;
	info.basePipelineIndex = 0;

	VkResult result = vkCreateGraphicsPipelines(
		context->device(),
		VK_NULL_HANDLE,
		1, &info,
		nullptr,
		&m_handle
	);
	if (result != VK_SUCCESS) {
		throw gpu_error{
			"Failed to create graphics pipeline."
		};
	}
}

}