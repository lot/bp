#define BP_API_IMPL
#include "vulkan_pipeline_layout.hpp"
#include "vulkan_context.hpp"
#include "vulkan_enum_util.hpp"
#include <bp/gpu/shader.hpp>
#include <bp/gpu/descriptor_type.hpp>
#include <bp/gpu/gpu_error.hpp>

using namespace std;

namespace bp {

vulkan_pipeline_layout::~vulkan_pipeline_layout() {
	if (m_handle != VK_NULL_HANDLE) {
		vkDestroyPipelineLayout(context->device(), m_handle, nullptr);
	}
	if (m_set_layout != VK_NULL_HANDLE) {
		vkDestroyDescriptorSetLayout(context->device(), m_set_layout, nullptr);
	}
}

void vulkan_pipeline_layout::init() {
	if (!m_descriptor_bindings.empty()) {
		VkDescriptorSetLayoutCreateInfo set_layout_info;
		set_layout_info.sType =
			VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
		set_layout_info.pNext = nullptr;
		set_layout_info.flags =
			VK_DESCRIPTOR_SET_LAYOUT_CREATE_PUSH_DESCRIPTOR_BIT_KHR;
		set_layout_info.bindingCount =
			static_cast<uint32_t>(m_descriptor_bindings.size());
		set_layout_info.pBindings = m_descriptor_bindings.data();

		VkResult result = vkCreateDescriptorSetLayout(
			context->device(),
			&set_layout_info,
			nullptr,
			&m_set_layout
		);
		if (result != VK_SUCCESS) {
			throw gpu_error{
				"Failed to create descriptor set layout."
			};
		}
	}

	VkPipelineLayoutCreateInfo info;
	info.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
	info.pNext = nullptr;
	info.flags = 0;
	info.setLayoutCount = m_set_layout == VK_NULL_HANDLE ? 0 : 1;
	info.pSetLayouts = &m_set_layout;
	info.pushConstantRangeCount =
		static_cast<uint32_t>(push_constant_ranges.size());
	info.pPushConstantRanges = push_constant_ranges.data();

	VkResult result = vkCreatePipelineLayout(
		context->device(),
		&info,
		nullptr,
		&m_handle
	);
	if (result != VK_SUCCESS) {
		if (m_set_layout != VK_NULL_HANDLE) {
			vkDestroyDescriptorSetLayout(
				context->device(),
				m_set_layout,
				nullptr
			);
			m_set_layout = VK_NULL_HANDLE;
		}
		throw gpu_error{
			"Failed to create pipeline layout."
		};
	}
}

static VkDescriptorType to_vulkan_descriptor_type(descriptor_type t) {
	switch (t) {
	case descriptor_type::sampler:
		return VK_DESCRIPTOR_TYPE_SAMPLER;
	case descriptor_type::combined_image_sampler:
		return VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
	case descriptor_type::sampled_image:
		return VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE;
	case descriptor_type::storage_image:
		return VK_DESCRIPTOR_TYPE_STORAGE_IMAGE;
	case descriptor_type::uniform_texel_buffer:
		return VK_DESCRIPTOR_TYPE_UNIFORM_TEXEL_BUFFER;
	case descriptor_type::storage_texel_buffer:
		return VK_DESCRIPTOR_TYPE_STORAGE_TEXEL_BUFFER;
	case descriptor_type::uniform_buffer:
		return VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
	case descriptor_type::storage_buffer:
		return VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
	case descriptor_type::uniform_buffer_dynamic:
		return VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC;
	case descriptor_type::storage_buffer_dynamic:
		return VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC;
	case descriptor_type::input_attachment:
		return VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT;
	default:
		throw gpu_error{
			"Unknown descriptor type."
		};
	}
}

void vulkan_pipeline_layout::add_push_constant_range(
	vector<shader_stage> shader_stages,
	uint32_t offset,
	uint32_t size
) {
	push_constant_ranges.emplace_back();
	auto& range = push_constant_ranges.back();
	range.stageFlags = to_vulkan_shader_stage_mask(shader_stages);
	range.offset = offset;
	range.size = size;
}

void vulkan_pipeline_layout::add_descriptor_binding(
	uint32_t binding,
	vector<shader_stage> shader_stages,
	descriptor_type type,
	uint32_t count
) {
	m_descriptor_bindings.emplace_back();
	auto& b = m_descriptor_bindings.back();
	b.binding = binding;
	b.descriptorType = to_vulkan_descriptor_type(type);
	b.descriptorCount = count;
	b.stageFlags = to_vulkan_shader_stage_mask(shader_stages);
	b.pImmutableSamplers = nullptr;
}

}