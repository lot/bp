#include <bp/gpu/draw_object.hpp>
#include "vulkan_command_recorder.hpp"
#include "vulkan_graphics_pipeline.hpp"
#include "vulkan_pipeline_layout.hpp"
#include "vulkan_buffer.hpp"

using namespace std;

namespace bp {

void vulkan_command_recorder::operator()(bind_pipeline_command& cmd) {
	if (cmd.pipeline->handle() == VK_NULL_HANDLE) cmd.pipeline->init();
	vkCmdBindPipeline(
		cmd_buf,
		VK_PIPELINE_BIND_POINT_GRAPHICS,
		cmd.pipeline->handle()
	);
	bound_pipeline = cmd.pipeline;
	vkCmdSetViewport(cmd_buf, 0, 1, viewport);
	vkCmdSetScissor(cmd_buf, 0, 1, scissor);
}

void vulkan_command_recorder::operator()(push_constant_command& cmd) {
	vkCmdPushConstants(
		cmd_buf,
		bound_pipeline->layout()->handle(),
		cmd.stage_flags,
		cmd.offset,
		static_cast<uint32_t>(cmd.data.size()),
		cmd.data.data()
	);
}

void vulkan_command_recorder::operator()(bind_vertex_buffers_command& cmd) {
	uint32_t binding_count = static_cast<uint32_t>(cmd.buffers.size());

	vector<VkBuffer> buffers;
	buffers.reserve(binding_count);
	for (auto& b : cmd.buffers) { buffers.push_back(b->handle()); }

	vkCmdBindVertexBuffers(
		cmd_buf,
		cmd.first_binding,
		binding_count,
		buffers.data(),
		cmd.offsets.data()
	);
}

void vulkan_command_recorder::operator()(bind_index_buffer_command& cmd) {
	vkCmdBindIndexBuffer(
		cmd_buf,
		cmd.buffer->handle(),
		cmd.offset,
		cmd.index_type == index_type::u16
			? VK_INDEX_TYPE_UINT16
			: VK_INDEX_TYPE_UINT32
	);
}

void vulkan_command_recorder::operator()(draw_command& cmd) {
	vkCmdDraw(
		cmd_buf,
		cmd.vertex_count,
		cmd.instance_count,
		cmd.first_vertex,
		cmd.first_instance
	);
}

void vulkan_command_recorder::operator()(draw_indexed_command& cmd) {
	vkCmdDrawIndexed(
		cmd_buf,
		cmd.index_count,
		cmd.instance_count,
		cmd.first_index,
		cmd.vertex_offset,
		cmd.first_instance
	);
}

}
