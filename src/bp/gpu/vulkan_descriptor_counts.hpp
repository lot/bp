#pragma once
#include <cstdint>

namespace bp {

struct vulkan_descriptor_counts {
	uint32_t sampler_count;
	uint32_t combined_image_sampler_count;
	uint32_t sampled_image_count;
	uint32_t storage_image_count;
	uint32_t uniform_texel_buffer_count;
	uint32_t storage_texel_buffer_count;
	uint32_t uniform_buffer_count;
	uint32_t storage_buffer_count;
	uint32_t uniform_buffer_dynamic_count;
	uint32_t storage_buffer_dynamic_count;
	uint32_t input_attachment_count;
};

}
