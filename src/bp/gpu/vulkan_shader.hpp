#pragma once
#include <bp/gpu/shader.hpp>
#include <vulkan/vulkan.h>
#include <memory>
#include <unordered_map>

#include <bp/util/api.hpp>

namespace bp {

class vulkan_context;

class BP_API vulkan_shader : public shader {
	std::shared_ptr<vulkan_context> context;
	shader_stage m_stage;
	VkPipelineShaderStageCreateInfo m_stage_info;
	std::unordered_map<std::string, shader_io> inputs;
	std::unordered_map<std::string, shader_io> outputs;

public:
	vulkan_shader(
		std::shared_ptr<vulkan_context> context,
		shader_stage stage,
		std::size_t spirv_code_size,
		const std::uint8_t* spirv_code
	);

	~vulkan_shader();

	shader_stage stage() const override { return m_stage; }

	shader_io get_input(const std::string& name) const override;

	shader_io get_output(const std::string& name) const override;

	const VkShaderModule handle() const { return m_stage_info.module; }

	const VkPipelineShaderStageCreateInfo& stage_info() const {
		return m_stage_info;
	}
};

}

#undef BP_API