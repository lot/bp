#pragma once
#include <bp/util/thread/semaphore.hpp>
#include <vulkan/vulkan.h>
#include <vector>
#include <queue>
#include <memory>
#include <mutex>
#include <future>
#include <functional>

#include <bp/util/api.hpp>

namespace bp {

class vulkan_command_pool;
class vulkan_command_buffer;

class BP_API vulkan_queue {
	friend class vulkan_command_pool;

	VkDevice device;
	VkQueue m_handle;
	uint32_t family_index;
	uint32_t index;
	semaphore cmd_pools_sem;
	std::queue<std::shared_ptr<vulkan_command_pool>> cmd_pool_queue;
	std::mutex cmd_pool_queue_mutex;

public:
	vulkan_queue(
		VkDevice device,
		uint32_t family_index,
		uint32_t index
	);

	std::future<std::shared_ptr<vulkan_command_buffer>> record_commands(
		VkCommandBufferLevel level,
		std::function<void(VkCommandBuffer)> record,
		VkCommandBufferUsageFlags usage_flags = 0,
		const VkCommandBufferInheritanceInfo* inheritance_info = nullptr
	);

	VkQueue handle() { return m_handle; }
};

}

#undef BP_API