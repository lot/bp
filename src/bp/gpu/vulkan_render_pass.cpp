#define BP_API_IMPL
#include "vulkan_render_pass.hpp"
#include "vulkan_context.hpp"
#include "vulkan_attachment.hpp"
#include "vulkan_subpass.hpp"
#include "vulkan_swapchain.hpp"
#include <bp/gpu/texture.hpp>
#include <bp/gpu/gpu_error.hpp>
#include <functional>
#include <queue>

using namespace std;

namespace bp {

vulkan_render_pass::vulkan_render_pass(shared_ptr<vulkan_context> context) :
	context{context} {}

vulkan_render_pass::~vulkan_render_pass() {
	vkDestroyRenderPass(context->device(), m_handle, nullptr);
}

shared_ptr<attachment> vulkan_render_pass::add_attachment(
	texture_format format,
	attachment_flags flags
) {
	auto attachment = make_shared<vulkan_attachment>(
		shared_from_this(),
		attachments.size(),
		flags
	);

	VkFormat vk_format;
	switch (format) {
	case texture_format::r8:
		vk_format = VK_FORMAT_R8_UNORM;
		break;
	case texture_format::r8g8:
		vk_format = VK_FORMAT_R8G8_UNORM;
		break;
	case texture_format::r8g8b8a8:
		vk_format = VK_FORMAT_R8G8B8A8_UNORM;
		break;
	case texture_format::depth:
		vk_format = VK_FORMAT_D16_UNORM;
		break;
	}

	attachment->format(vk_format);

	attachments.push_back(attachment);
	return attachment;
}

shared_ptr<attachment> vulkan_render_pass::add_attachment(
	shared_ptr<bp::swapchain> swapchain,
	attachment_flags flags
) {
	auto attachment = make_shared<vulkan_attachment>(
		shared_from_this(),
		attachments.size(),
		flags
	);
	attachment->format(
		static_pointer_cast<vulkan_swapchain>(swapchain)->format()
	);

	attachments.push_back(attachment);
	return attachment;
}

shared_ptr<subpass> vulkan_render_pass::add_subpass() {
	auto subpass = make_shared<vulkan_subpass>(shared_from_this());
	subpasses.emplace_back(subpass);
	return subpass;
}

void vulkan_render_pass::order_subpasses() {
	//Use breadth first search traversal to define the correct order of
	//subpasses. Vulkan expects for subpass dependencies that dst > src.
	vector<shared_ptr<vulkan_subpass>> ordered;
	queue<shared_ptr<vulkan_subpass>> q;

	for (auto& s : subpasses) {
		if (s->state & vulkan_subpass::source_subpass) {
			s->state |= vulkan_subpass::visited;
			q.push(s);
		}
	}

	while (!q.empty()) {
		auto& s = q.front();
		q.pop();
		s->index = static_cast<uint32_t>(ordered.size());
		ordered.push_back(s);

		for (auto& d : s->dependents) {
			if (!(d->state & vulkan_subpass::visited)) {
				d->state |= vulkan_subpass::visited;
				q.push(d);
			}
		}
	}

	subpasses = move(ordered);
}

bool vulkan_render_pass::resolve_dependency_src_access(
	std::shared_ptr<bp::vulkan_subpass> src,
	std::shared_ptr<bp::vulkan_attachment> attachment,
	VkAccessFlags& access
) {
	bool common = false;
	for (auto& src_color : src->color_attachments) {
		if (attachment == src_color) {
			common = true;
			access |= VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
			break;
		}
	}
	for (auto& src_resolve : src->resolve_attachments) {
		if (attachment == src_resolve) {
			common = true;
			access |= VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
			break;
		}
	}
	if (attachment == src->depth_attachment) {
		common = true;
		access |= VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
	}

	for (auto& src_preserve : src->preserve_attachments) {
		if (attachment == src_preserve) {
			common = true;
		}
	}

	return common;
}

pair<VkAccessFlags, VkAccessFlags>
vulkan_render_pass::resolve_dependency_access(
	shared_ptr<vulkan_subpass> src,
	shared_ptr<vulkan_subpass> dst
) {
	//Figure out the access masks for subpass dependencies based upon the
	//attachments the subpasses have in common
	auto access = make_pair<VkAccessFlags, VkAccessFlags>(0, 0);

	for (auto& input : dst->input_attachments) {
		if (resolve_dependency_src_access(src, input, access.first)) {
			access.second |= VK_ACCESS_INPUT_ATTACHMENT_READ_BIT;
		}
	}

	for (auto& color : dst->color_attachments) {
		if (resolve_dependency_src_access(src, color, access.first)) {
			access.second |= VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
		}
	}

	for (auto& color : dst->resolve_attachments) {
		if (resolve_dependency_src_access(src, color, access.first)) {
			access.second |= VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
		}
	}

	if (resolve_dependency_src_access(
		src,
		dst->depth_attachment,
		access.first
	)) {
		access.second |= VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
	}

	for (auto& preserve : dst->preserve_attachments) {
		resolve_dependency_src_access(src, preserve, access.first);
	}

	return access;
}

void vulkan_render_pass::init() {
	if (subpasses.empty()) {
		throw gpu_error{
			"No subpasses added to the render pass."
		};
	}

	vector<VkAttachmentDescription> attachment_descriptions;
	for (auto& a : attachments) {
		attachment_descriptions.emplace_back();
		auto& d = attachment_descriptions.back();
		d.flags = 0;
		d.format = a->format();

		if (d.format == VK_FORMAT_UNDEFINED) {
			throw gpu_error{
				"Undefined format for attachment " + to_string(a->index()) + "."
			};
		}

		//TODO support multisampling
		d.samples = VK_SAMPLE_COUNT_1_BIT;

		if (a->flags() & attachment_flags::clear) {
			d.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
		} else if (a->flags() & attachment_flags::load) {
			d.loadOp = VK_ATTACHMENT_LOAD_OP_LOAD;
		} else {
			d.loadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
		}

		if (a->flags() & attachment_flags::store) {
			d.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
		} else {
			d.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
		}

		if (a->flags() & vulkan_attachment::depth_attachment) {
			d.stencilLoadOp = d.loadOp;
			d.stencilStoreOp = d.storeOp;
			d.initialLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
		} else {
			d.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
			d.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
			d.initialLayout = a->flags() & vulkan_attachment::color_attachment
							  ? VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
							  : VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		}
		d.finalLayout = a->flags() & vulkan_attachment::input_attachment
						? VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
						: d.initialLayout;
	}

	order_subpasses();

	struct subpass_attachments {
		vector<VkAttachmentReference> input;
		vector<VkAttachmentReference> color;
		vector<VkAttachmentReference> resolve;
		VkAttachmentReference depth;
		vector<uint32_t> preserve;
	};

	vector<subpass_attachments> subpass_attachments;
	vector<VkSubpassDescription> subpass_descriptions;
	vector<VkSubpassDependency> subpass_dependency_descriptions;

	for (auto& s : subpasses) {
		subpass_descriptions.emplace_back();
		auto& subpass_description = subpass_descriptions.back();
		subpass_description.flags = 0;
		subpass_description.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;

		subpass_attachments.emplace_back();
		auto& sa = subpass_attachments.back();

		for (auto& a : s->input_attachments) {
			sa.input.emplace_back();
			auto& ref = sa.input.back();
			ref.attachment = a->index();
			ref.layout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
		}
		subpass_description.inputAttachmentCount =
			static_cast<uint32_t>(sa.input.size());
		subpass_description.pInputAttachments = sa.input.data();

		for (auto& a : s->color_attachments) {
			sa.color.emplace_back();
			auto& ref = sa.color.back();
			ref.attachment = a->index();
			ref.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
		}
		subpass_description.colorAttachmentCount =
			static_cast<uint32_t>(sa.color.size());
		subpass_description.pColorAttachments = sa.color.data();


		for (auto& a : s->resolve_attachments) {
			sa.resolve.emplace_back();
			auto& ref = sa.resolve.back();
			ref.attachment = a ? a->index() : VK_ATTACHMENT_UNUSED;
			ref.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
		}
		subpass_description.pResolveAttachments = sa.resolve.data();

		if (s->depth_attachment) {
			sa.depth.attachment =
				s->depth_attachment->index();
			sa.depth.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
			subpass_description.pDepthStencilAttachment = &sa.depth;
		} else {
			subpass_description.pDepthStencilAttachment = nullptr;
		}

		for (auto& a : s->preserve_attachments) {
			sa.preserve.push_back(a->index());
		}
		subpass_description.preserveAttachmentCount =
			static_cast<uint32_t>(sa.preserve.size());
		subpass_description.pPreserveAttachments = sa.preserve.data();

		for (auto& d : s->dependencies) {
			auto src = d.src.lock();
			auto access = resolve_dependency_access(src, s);
			subpass_dependency_descriptions.emplace_back();
			auto& subpass_dependency = subpass_dependency_descriptions.back();

			subpass_dependency.srcSubpass = src->index;
			subpass_dependency.dstSubpass = s->index;
			subpass_dependency.srcStageMask = d.src_stage;
			subpass_dependency.dstStageMask = d.dst_stage;
			subpass_dependency.srcAccessMask = access.first;
			subpass_dependency.dstAccessMask = access.second;
			subpass_dependency.dependencyFlags = 0;
		}
	}

	VkRenderPassCreateInfo create_info;
	create_info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
	create_info.pNext = nullptr;
	create_info.flags = 0;
	create_info.attachmentCount =
		static_cast<uint32_t>(attachment_descriptions.size());
	create_info.pAttachments = attachment_descriptions.data();
	create_info.subpassCount =
		static_cast<uint32_t>(subpass_descriptions.size());
	create_info.pSubpasses = subpass_descriptions.data();
	create_info.dependencyCount =
		static_cast<uint32_t>(subpass_dependency_descriptions.size());
	create_info.pDependencies = subpass_dependency_descriptions.data();

	VkResult result = vkCreateRenderPass(
		context->device(),
		&create_info,
		nullptr,
		&m_handle
	);
	if (result != VK_SUCCESS) {
		throw gpu_error{
			"Failed to create render pass."
		};
	}
}

}