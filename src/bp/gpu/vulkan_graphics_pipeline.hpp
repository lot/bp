#pragma once
#include <bp/gpu/pipeline.hpp>
#include <vulkan/vulkan.h>
#include <memory>
#include <vector>

#include <bp/util/api.hpp>

namespace bp {

class vulkan_context;
class vulkan_pipeline_layout;
class vulkan_subpass;
class vulkan_shader;

class BP_API vulkan_graphics_pipeline : public pipeline {
	std::shared_ptr<vulkan_context> context;
	std::shared_ptr<vulkan_pipeline_layout> m_layout;
	std::vector<std::shared_ptr<vulkan_shader>> shaders;
	std::shared_ptr<vulkan_subpass> subpass;
	std::vector<VkVertexInputBindingDescription> vk_vertex_bindings;
	std::vector<VkVertexInputAttributeDescription> vk_vertex_attributes;
	VkPrimitiveTopology topology;
	VkPolygonMode polygon_mode;
	graphics_pipeline_flags flags;
	VkPipeline m_handle;

public:
	vulkan_graphics_pipeline(
		std::shared_ptr<vulkan_context> context,
		std::shared_ptr<vulkan_pipeline_layout> layout,
		std::vector<std::shared_ptr<vulkan_shader>> shaders,
		std::shared_ptr<vulkan_subpass> subpass,
		std::vector<vertex_input_binding> vertex_bindings,
		std::vector<vertex_input_attribute> vertex_attributes,
		primitive_topology topology,
		bp::polygon_mode polygon_mode,
		graphics_pipeline_flags flags
	);
	~vulkan_graphics_pipeline();

	void init();

	VkPipeline handle() { return m_handle; }

	std::shared_ptr<vulkan_pipeline_layout> layout() { return m_layout; }
};

}

#undef BP_API