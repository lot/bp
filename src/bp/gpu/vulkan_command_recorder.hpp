#pragma once
#include "vulkan_subpass_command.hpp"
#include <vulkan/vulkan.h>

namespace bp {

class vulkan_command_recorder {
	std::shared_ptr<vulkan_graphics_pipeline> bound_pipeline;
	VkCommandBuffer cmd_buf;
	const VkViewport* viewport;
	const VkRect2D* scissor;
public:
	explicit vulkan_command_recorder(
		VkCommandBuffer cmd_buf,
		const VkViewport& viewport,
		const VkRect2D& scissor
	) :
		cmd_buf{cmd_buf},
		viewport{&viewport},
		scissor{&scissor}
	{}

	void operator()(bind_pipeline_command& cmd);
	void operator()(push_constant_command& cmd);
	void operator()(bind_vertex_buffers_command& cmd);
	void operator()(bind_index_buffer_command& cmd);
	void operator()(draw_command& cmd);
	void operator()(draw_indexed_command& cmd);
};

}
