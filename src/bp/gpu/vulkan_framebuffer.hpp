#pragma once
#include "vulkan_target.hpp"
#include <bp/util/event.hpp>
#include <bp/gpu/framebuffer.hpp>
#include <vulkan/vulkan.h>
#include <vector>

#include <bp/util/api.hpp>

namespace bp {

class vulkan_context;
class vulkan_attachment;
class vulkan_render_pass;
class vulkan_swapchain;

class BP_API vulkan_framebuffer : public framebuffer {
	std::shared_ptr<vulkan_context> context;
	std::shared_ptr<vulkan_render_pass> render_pass;
	std::shared_ptr<vulkan_swapchain> m_swapchain;
	std::size_t swapchain_index;
	std::size_t m_width;
	std::size_t m_height;
	std::vector<VkFramebuffer> handles;

	struct binding {
		std::shared_ptr<vulkan_attachment> attachment;
		std::shared_ptr<vulkan_target> target;

		binding() = default;
		binding(
			std::shared_ptr<vulkan_attachment> a,
			std::shared_ptr<vulkan_target> t
		) : attachment{a}, target{t} {}
	};
	std::vector<binding> bindings;

public:
	vulkan_framebuffer(
		std::shared_ptr<vulkan_context> context,
		std::shared_ptr<vulkan_render_pass> render_pass,
		std::size_t width,
		std::size_t height
	);
	~vulkan_framebuffer();

	std::size_t width() const override { return m_width; }

	std::size_t height() const override { return m_height; }

	void resize(std::size_t width, std::size_t height) override;

	void bind(
		std::shared_ptr<bp::attachment> attachment,
		std::shared_ptr<bp::texture> texture
	) override;

	void bind(
		std::shared_ptr<bp::attachment> attachment,
		std::shared_ptr<bp::swapchain> swapchain
	) override;

	void init();

	bool ready() const { return !handles.empty(); }

	std::vector<VkClearValue> clear_values();

	VkFramebuffer current_handle();

	uint32_t current_frame_index() const;

	uint32_t frame_count() const;

	std::shared_ptr<vulkan_swapchain> swapchain() { return m_swapchain; }

	void before_transitions(VkCommandBuffer cmd_buffer);

	void after_transitions(VkCommandBuffer cmd_buffer);

	event<std::size_t, std::size_t> resize_event;
};

}

#undef BP_API