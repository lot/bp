#pragma once
#include <vulkan/vulkan.h>
#include <memory>
#include <vector>
#include <variant>

namespace bp {

class vulkan_graphics_pipeline;
class vulkan_buffer;
enum class index_type;

struct bind_pipeline_command {
	std::shared_ptr<vulkan_graphics_pipeline> pipeline;
};

struct push_constant_command {
	VkShaderStageFlags stage_flags;
	uint32_t offset;
	std::vector<uint8_t> data;
};

struct bind_vertex_buffers_command {
	std::vector<std::shared_ptr<vulkan_buffer>> buffers;
	std::vector<VkDeviceSize> offsets;
	uint32_t first_binding;
};

struct bind_index_buffer_command {
	std::shared_ptr<vulkan_buffer> buffer;
	size_t offset;
	bp::index_type index_type;
};

struct draw_command {
	uint32_t vertex_count;
	uint32_t instance_count;
	uint32_t first_vertex;
	uint32_t first_instance;
};

struct draw_indexed_command {
	uint32_t index_count;
	uint32_t instance_count;
	uint32_t first_index;
	int32_t vertex_offset;
	uint32_t first_instance;
};

using vulkan_subpass_command =
	std::variant<
	    bind_pipeline_command,
	    push_constant_command,
	    bind_vertex_buffers_command,
	    bind_index_buffer_command,
	    draw_command,
	    draw_indexed_command
	>;

}
