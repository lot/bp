#define BP_API_IMPL
#include "vulkan_framebuffer.hpp"
#include "vulkan_context.hpp"
#include "vulkan_attachment.hpp"
#include "vulkan_swapchain.hpp"
#include "vulkan_texture.hpp"
#include "vulkan_render_pass.hpp"
#include <bp/gpu/gpu_error.hpp>

using namespace std;

namespace bp {

vulkan_framebuffer::vulkan_framebuffer(
	shared_ptr<bp::vulkan_context> context,
	shared_ptr<bp::vulkan_render_pass> render_pass,
	size_t width,
	size_t height
) : context{context},
	render_pass{render_pass},
	swapchain_index{0},
	m_width{width},
	m_height{height} {}

vulkan_framebuffer::~vulkan_framebuffer() {
	for (auto h : handles) vkDestroyFramebuffer(context->device(), h, nullptr);
}

void vulkan_framebuffer::resize(size_t width, size_t height) {
	for (auto h : handles) vkDestroyFramebuffer(context->device(), h, nullptr);
	m_width = width;
	m_height = height;
	for (auto& b : bindings) b.target->resize_target(width, height);
	init();
	resize_event(width, height);
}

void vulkan_framebuffer::bind(
	shared_ptr<bp::attachment> attachment,
	shared_ptr<bp::texture> texture
) {
	auto a = static_pointer_cast<vulkan_attachment>(attachment);
	if (a->render_pass.lock() != render_pass) {
		throw gpu_error{
			"Can not bind attachment created from a different render pass "
			"than passed upon framebuffer creation."
		};
	}

	auto t = static_pointer_cast<vulkan_texture>(texture);
	bindings.emplace_back(a, t);
}

void vulkan_framebuffer::bind(
	shared_ptr<bp::attachment> attachment,
	shared_ptr<bp::swapchain> swapchain
) {
	if (vulkan_framebuffer::m_swapchain) {
		throw gpu_error{
			"Can only bind one swapchain per framebuffer."
		};
	}

	auto a = static_pointer_cast<vulkan_attachment>(attachment);
	if (a->render_pass.lock() != render_pass) {
		throw gpu_error{
			"Can not bind attachment created from a different render pass "
			"than passed upon framebuffer creation."
		};
	}

	auto s = static_pointer_cast<vulkan_swapchain>(swapchain);
	bindings.emplace_back(a, s);
	vulkan_framebuffer::m_swapchain = s;
	swapchain_index = a->index();
}

void vulkan_framebuffer::init() {
	vector<VkImageView> attachments{
		render_pass->attachments.size(),
		VK_NULL_HANDLE
	};

	for (auto& b : bindings) {
		size_t index = b.attachment->index();
		if (m_swapchain && b.attachment->index() == swapchain_index) continue;
		if (attachments[index] != VK_NULL_HANDLE) {
			throw gpu_error{
				"Duplicate bindings to the same attachment."
			};
		}
		auto t = static_pointer_cast<vulkan_texture>(b.target);
		attachments[index] = t->view;
	}

	for (size_t i = 0; i < attachments.size(); i++) {
		if (attachments[i] == VK_NULL_HANDLE
			&& !(m_swapchain && swapchain_index == i)) {
			throw gpu_error{
				"Missing attachment(s). All attachments must be bound to the "
				"framebuffer."
			};
		}
	}

	VkFramebufferCreateInfo info;
	info.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
	info.pNext = nullptr;
	info.flags = 0;
	info.renderPass = render_pass->handle();
	info.attachmentCount = static_cast<uint32_t>(attachments.size());
	info.pAttachments = attachments.data();
	info.width = static_cast<uint32_t>(m_width);
	info.height = static_cast<uint32_t>(m_height);
	info.layers = 1;

	size_t fbcount = m_swapchain ? m_swapchain->image_count : 1;
	handles.resize(fbcount);
	for (size_t i = 0; i < fbcount; i++) {
		if (m_swapchain) {
			attachments[swapchain_index] = m_swapchain->image_views[i];
		}
		VkResult result = vkCreateFramebuffer(
			context->device(),
			&info,
			nullptr,
			handles.data() + i
		);

		if (result != VK_SUCCESS) {
			for (size_t j = 0; j < i; j++) {
				vkDestroyFramebuffer(context->device(), handles[j], nullptr);
			}
			handles.clear();
			throw gpu_error{
				"Failed to create framebuffer."
			};
		}
	}
}

vector<VkClearValue> vulkan_framebuffer::clear_values() {
	vector<VkClearValue> clear_values;
	clear_values.reserve(bindings.size());
	for (auto& b : bindings) {
		clear_values.push_back(b.attachment->clear_value());
	}
	return clear_values;
}

VkFramebuffer vulkan_framebuffer::current_handle() {
	return handles[current_frame_index()];
}

uint32_t vulkan_framebuffer::current_frame_index() const {
	if (m_swapchain) {
		return m_swapchain->current_image_index;
	}
	else return 0;
}

uint32_t vulkan_framebuffer::frame_count() const {
	return static_cast<uint32_t>(handles.size());
}

void vulkan_framebuffer::before_transitions(VkCommandBuffer cmd_buffer) {
	vector<VkImageMemoryBarrier> image_barriers;
	VkPipelineStageFlags dst_stage = 0;

	for (auto& b : bindings) {
		auto barrier = b.target->before_transition(b.attachment);
		if (barrier) {
			image_barriers.push_back(barrier.value());
			if (b.attachment->flags() & vulkan_attachment::color_attachment) {
				dst_stage |= VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
			} else if (b.attachment->flags()
					   & vulkan_attachment::depth_attachment) {
				dst_stage |= VK_PIPELINE_STAGE_LATE_FRAGMENT_TESTS_BIT;
			} else if (b.attachment->flags()
					   & vulkan_attachment::input_attachment) {
				dst_stage |= VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT;
			}
		}
	}

	if (image_barriers.empty()) return;

	vkCmdPipelineBarrier(
		cmd_buffer,
		VK_PIPELINE_STAGE_ALL_COMMANDS_BIT,
		dst_stage,
		0,
		0, nullptr,
		0, nullptr,
		static_cast<uint32_t>(image_barriers.size()), image_barriers.data()
	);
}

void vulkan_framebuffer::after_transitions(VkCommandBuffer cmd_buffer) {
	vector<VkImageMemoryBarrier> image_barriers;

	for (auto& b : bindings) {
		auto barrier = b.target->after_transition(b.attachment);
		if (barrier) {
			image_barriers.push_back(barrier.value());
		}
	}

	if (image_barriers.empty()) return;

	vkCmdPipelineBarrier(
		cmd_buffer,
		VK_PIPELINE_STAGE_ALL_COMMANDS_BIT,
		VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT,
		0,
		0, nullptr,
		0, nullptr,
		static_cast<uint32_t>(image_barriers.size()), image_barriers.data()
	);
}

}