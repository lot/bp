#pragma once
#include "vulkan_target.hpp"
#include "vk_mem_alloc.h"
#include <bp/gpu/texture.hpp>
#include <memory>
#include <vulkan/vulkan.h>

#include <bp/util/api.hpp>

namespace bp {

class vulkan_context;
class vulkan_buffer;
class vulkan_framebuffer;

class BP_API vulkan_texture : public texture, public vulkan_target {
	friend class vulkan_framebuffer;

	std::shared_ptr<vulkan_context> context;
	texture_type m_type;
	texture_format m_format;
	texture_usage m_usage;
	std::size_t m_width, m_height, m_depth;
	std::size_t m_size;
	VkImageCreateInfo image_info;
	VmaMemoryUsage memory_usage;
	VkImage image;
	VkImageView view;
	VmaAllocation memory_allocation;
	VkMemoryPropertyFlags memory_property_flags;
	std::unique_ptr<vulkan_buffer> staging_buffer;
	std::uint8_t* mapped;
	VkImageLayout layout;
	VkAccessFlags access_flags;
	VkImageMemoryBarrier transition_barrier;

	void create();
	void destroy();

public:
	vulkan_texture(
		std::shared_ptr<vulkan_context> context,
		texture_type type,
		texture_format format,
		texture_usage usage,
		std::size_t width,
		std::size_t height,
		std::size_t depth,
		VmaMemoryUsage memory_usage,
		VkImageTiling image_tiling
	);

	~vulkan_texture();

	texture_type type() const override { return m_type; }

	texture_format format() const override { return m_format; }

	texture_usage usage() const override { return m_usage; }

	std::size_t width() const override { return m_width; }

	std::size_t height() const override { return m_height; }

	std::size_t depth() const override { return m_depth; }

	std::size_t size() const override { return m_size; }

	void resize(
		std::size_t width,
		std::size_t height,
		std::size_t depth
	) override;

	std::uint8_t* map() override;

	void unmap() override;

	const VkImageMemoryBarrier& transition(
		VkImageLayout dst_layout,
		VkAccessFlags dst_access
	);

	void resize_target(std::size_t width, std::size_t height) override {
		resize(width, height, 1);
	}

	std::optional<VkImageMemoryBarrier> before_transition(
		std::shared_ptr<vulkan_attachment> attachment
	) override;

	VkImage image_handle() { return image; }

	VkBuffer staging_buffer_handle();
};

}

#undef BP_API