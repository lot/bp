#pragma once
#include <bp/gpu/draw_object.hpp>
#include "vulkan_subpass_command.hpp"
#include <memory>
#include <future>
#include <vector>
#include <vulkan/vulkan.h>

#include <bp/util/api.hpp>

namespace bp {

class vulkan_subpass_instance;
class vulkan_command_buffer;
class vulkan_queue;
class vulkan_graphics_pipeline;
class vulkan_buffer;

class BP_API vulkan_draw_object : public draw_object {
	std::weak_ptr<vulkan_subpass_instance> subpass_instance;
	std::vector<std::shared_ptr<vulkan_command_buffer>> cmd_buffers;
	std::vector<vulkan_subpass_command> commands;

public:
	vulkan_draw_object(
		std::weak_ptr<vulkan_subpass_instance> subpass_instance,
		uint32_t frame_count
	);

	std::future<std::shared_ptr<vulkan_command_buffer>> prepare(
		std::shared_ptr<vulkan_queue> queue,
		VkViewport viewport,
		VkRect2D scissor,
		const VkCommandBufferInheritanceInfo& inheritance_info,
		uint32_t frame_index
	);

	void bind_pipeline(std::shared_ptr<bp::pipeline> pipeline) override;

	void push_constants(
		uint32_t offset,
		uint32_t size,
		const void* data,
		std::vector<shader_stage> shader_stages
	) override;

	void bind_vertex_buffers(
		std::vector<vertex_buffer_binding> bindings,
		uint32_t first_binding
	) override;

	void bind_index_buffer(
		std::shared_ptr<bp::buffer> buffer,
		size_t offset,
		bp::index_type index_type
	) override;

	void draw(
		uint32_t vertex_count,
		uint32_t instance_count,
		uint32_t first_vertex,
		uint32_t first_instance
	) override;

	void draw_indexed(
		uint32_t index_count,
		uint32_t instance_count,
		uint32_t first_index,
		int32_t vertex_offset,
		uint32_t first_instance
	) override;

	void dirty();
};

}

#undef BP_API