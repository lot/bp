#pragma once
#include "vulkan_target.hpp"
#include <bp/gpu/swapchain.hpp>
#include <bp/util/event.hpp>
#include <vulkan/vulkan.h>
#include <memory>
#include <vector>

#include <bp/util/api.hpp>

namespace bp {

class vulkan_context;
class vulkan_surface;
class vulkan_semaphore;
class vulkan_framebuffer;

class BP_API vulkan_swapchain : public swapchain, public vulkan_target {
	friend class vulkan_framebuffer;

	std::shared_ptr<vulkan_context> context;
	std::shared_ptr<vulkan_surface> surface;
	swapchain_flags m_flags;
	std::size_t m_width, m_height;
	VkSurfaceFormatKHR m_format;
	std::size_t image_count;
	VkSwapchainKHR handle;
	std::uint32_t current_image_index;
	std::vector<VkImage> images;
	std::vector<VkImageLayout> image_layouts;
	std::vector<VkAccessFlags> image_access_masks;
	std::vector<VkImageView> image_views;
	std::shared_ptr<vulkan_semaphore> image_available_sem;
	std::shared_ptr<vulkan_semaphore> present_wait_sem;

	void create();

	VkImageMemoryBarrier current_image_transition(
		VkImageLayout dst_layout,
		VkAccessFlags dst_access
	);

public:
	vulkan_swapchain(
		std::shared_ptr<vulkan_context> context,
		std::shared_ptr<vulkan_surface> surface,
		swapchain_flags flags,
		std::size_t width,
		std::size_t height
	);

	~vulkan_swapchain();

	swapchain_flags flags() const override { return m_flags; }

	size_t width() const override { return m_width; }

	size_t height() const override { return m_height; }

	void resize(std::size_t width, std::size_t height) override;

	VkFormat format() const { return m_format.format; }

	void next_image();

	void swap() override;

	void present_wait_semaphore(std::shared_ptr<vulkan_semaphore> sem) {
		present_wait_sem = sem;
	}

	std::shared_ptr<vulkan_semaphore> image_available_semaphore() {
		return image_available_sem;
	}

	void resize_target(std::size_t width, std::size_t height) override {
		resize(width, height);
	}

	std::optional<VkImageMemoryBarrier> before_transition(
		std::shared_ptr<vulkan_attachment> attachment
	) override;

	std::optional<VkImageMemoryBarrier> after_transition(
		std::shared_ptr<vulkan_attachment> attachment
	) override;

	event<> swap_event;
	event<> invalidated_event;
};

}

#undef BP_API