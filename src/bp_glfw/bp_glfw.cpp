#define GLFW_INCLUDE_VULKAN
#include "../bp/gpu/vulkan_instance.hpp"
#include "../bp/gpu/vulkan_surface.hpp"
#include <bp_glfw/bp_glfw.hpp>
#include <bp/gpu/gpu_error.hpp>
#include <vector>

using namespace std;

namespace bp_glfw {

shared_ptr<bp::instance> create_instance() {
	uint32_t count;
	auto glfw_extensions = glfwGetRequiredInstanceExtensions(&count);
	vector<const char*> extensions{glfw_extensions, glfw_extensions + count};
	return make_shared<bp::vulkan_instance>(move(extensions));
}

shared_ptr<bp::surface> create_surface(
	std::shared_ptr<bp::instance> instance,
	GLFWwindow* window
) {
	auto vk_instance = static_pointer_cast<bp::vulkan_instance>(instance);
	VkSurfaceKHR surface_handle;
	VkResult result = glfwCreateWindowSurface(
		vk_instance->handle(),
		window,
		nullptr,
		&surface_handle
	);
	if (result != VK_SUCCESS) {
		throw bp::gpu_error{
			"Failed to create window surface."
		};
	}

	return make_shared<bp::vulkan_surface>(vk_instance, surface_handle);
}

}