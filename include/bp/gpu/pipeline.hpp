#pragma once
#include <cstdint>

namespace bp {

enum class shader_data_type;

enum class pipeline_stage {
	top,
	vertex_input,
	vertex_shader,
	tesselation_control_shader,
	tesselation_evaluation_shader,
	geometry_shader,
	fragment_shader,
	early_fragment_tests,
	late_fragment_tests,
	color_attachment_output
};

enum class primitive_topology {
	point_list,
	line_list,
	line_strip,
	triangle_list,
	triangle_strip,
	triangle_fan,
	line_list_with_adjacency,
	line_strip_with_adjacency,
	triangle_list_with_adjacency,
	triangle_strip_with_adjacency,
	patch_list
};

enum class input_rate {
	vertex,
	instance
};

enum class polygon_mode {
	fill,
	line,
	point
};

struct vertex_input_binding {
	uint32_t binding{0};
	uint32_t stride{0};
	bp::input_rate input_rate{bp::input_rate::vertex};
};

struct vertex_input_attribute {
	uint32_t location;
	uint32_t binding;
	shader_data_type type;
	uint32_t offset;
};

class graphics_pipeline_flags {
	int m_flags;
public:
	enum {
		cull_front_face = 0x0001,
		cull_back_face = 0x0002,
		depth_test = 0x0004,
		primitive_restart = 0x0008
	};

	graphics_pipeline_flags() : m_flags{0} {}
	graphics_pipeline_flags(int flags) : m_flags{flags} {}
	operator int() const { return m_flags; }
	int operator=(int flags) { return (m_flags = flags); }
	int operator|=(int flags) { return (m_flags |= flags); }
};

class pipeline {
public:
	virtual ~pipeline() = default;
};

}