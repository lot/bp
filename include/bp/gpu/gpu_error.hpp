#pragma once
#include <stdexcept>

namespace bp {

class gpu_error : public std::runtime_error {
public:
	gpu_error(const std::string& what) : std::runtime_error{what} {}
};

}