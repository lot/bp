#pragma once
#include <memory>

namespace bp {

class draw_object;

class subpass_instance {
public:
	virtual ~subpass_instance() = default;

	virtual std::shared_ptr<bp::draw_object> add_draw_object() = 0;
};

}