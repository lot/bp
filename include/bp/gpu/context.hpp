#pragma once
#include <memory>
#include <cstdint>
#include <vector>
#include "texture.hpp"

namespace bp {

//Forward declarations

//bp/gpu/buffer.hpp
class buffer;
class buffer_usage;

//bp/gpu/texture.hpp
class texture;
enum class texture_format;
class texture_usage;

//bp/gpu/swapchain.hpp
class swapchain;
class swapchain_flags;

//bp/gpu/surface.hpp
class surface;

//bp/gpu/shader.hpp
class shader;
enum class shader_stage;

//bp/gpu/pipeline_layout.hpp
class pipeline_layout;

//bp/gpu/pipeline.hpp
enum class input_rate;
struct vertex_input_binding;
struct vertex_input_attribute;
enum class primitive_topology;
enum class polygon_mode;
class graphics_pipeline_flags;
class pipeline;

//bp/gpu/render_pass.hpp
class render_pass;

//bp/gpu/subpass.hpp
class subpass;

//bp/gpu/framebuffer.hpp
class framebuffer;

//bp/gpu/batch.hpp
class batch;

/**
 * Context providing GPU resources and the ability to execute GPU commands.
 *
 * The context provides an interface to the functionality provided by a GPU.
 * From a context you can setup resources like buffers and textures. You can
 * execute GPU commands with batches.
 */
class context {
public:
	virtual ~context() = default;

	/**
	 * Create a GPU buffer.
	 *
	 * Create a GPU buffer that can be accessed by the GPU.
	 *
	 * @param usage Usage flags specifying how the buffer should be accessable.
	 * @param size The size of the buffer in bytes.
	 * @return The created buffer.
	 */
	virtual std::shared_ptr<buffer> create_buffer(
		buffer_usage usage,
		std::size_t size
	) = 0;

	virtual std::shared_ptr<texture> create_texture(
		texture_type type,
		texture_format format,
		texture_usage usage,
		std::size_t width,
		std::size_t height,
		std::size_t depth = 1
	) = 0;

	virtual std::shared_ptr<swapchain> create_swapchain(
		std::shared_ptr<surface> target,
		swapchain_flags flags,
		std::size_t width,
		std::size_t height
	) = 0;

	virtual std::shared_ptr<render_pass> create_render_pass() = 0;

	virtual std::shared_ptr<framebuffer> create_framebuffer(
		std::shared_ptr<bp::render_pass> render_pass,
		std::size_t width,
		std::size_t height
	) = 0;

	virtual std::shared_ptr<batch> create_batch() = 0;

	virtual std::shared_ptr<shader> create_shader(
		shader_stage stage,
		std::size_t spirv_code_size,
		const std::uint8_t* spirv_code
	) = 0;

	virtual std::shared_ptr<pipeline_layout> create_pipeline_layout() = 0;

	virtual std::shared_ptr<pipeline> create_graphics_pipeline(
		std::shared_ptr<pipeline_layout> layout,
		std::vector<std::shared_ptr<shader>> shaders,
		std::shared_ptr<bp::subpass> subpass,
		std::vector<vertex_input_binding> vertex_bindings,
		std::vector<vertex_input_attribute> vertex_attributes,
		primitive_topology topology,
		bp::polygon_mode polygon_mode,
		graphics_pipeline_flags flags
	) = 0;
};

}