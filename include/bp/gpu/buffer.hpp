#pragma once
#include <cstdint>

namespace bp {

/**
 * Bitmask of buffer usage flags.
 *
 * Example usage:
 * @code
 * bp::buffer_usage usage =
 *     bp::buffer_usage::vertex | bp::buffer_usage::storage;
 * @endcode
 */
class buffer_usage {
	int m_flags;
public:
	/**
	 * Enumeration of buffer usage flags.
	 *
	 * This enumeration defines the possible flags to specify the desired
	 * capability of a GPU buffer.
	 */
	enum buffer_usage_bits {
		uniform = 0x0001,	/**< Specifies uniform buffer usage. */
		storage = 0x0002,	/**< Specifies storage buffer usage. */
		index = 0x0004,		/**< Specifies index buffer usage. */
		vertex = 0x0008		/**< Specifies vertex buffer usage. */
	};

	buffer_usage() : m_flags{0} {}
	buffer_usage(int flags) : m_flags{flags} {}
	operator int() const { return m_flags; }
	int operator=(int flags) { return (m_flags = flags); }
	int operator|=(int flags) { return (m_flags |= flags); }
};

/**
 * Buffer representation.
 *
 * This class represents GPU buffer created from a context.
 */
class buffer {
public:
	virtual ~buffer() = default;

	virtual buffer_usage usage() const = 0;

	virtual size_t size() const = 0;

	/**
	 * Map the buffer for reading and writing.
	 *
	 * This method maps the GPU buffer to an opaque pointer for read/write
	 * access. If the buffer memory resides on the GPU, it must be uploaded to
	 * the GPU with a batch in order to access changes written to the mapped
	 * pointer. Likewise, the buffer must be downloaded from the GPU in order
	 * to read changes written by the GPU from the mapped pointer.
	 *
	 * @return The opaque pointer to the buffer memory.
	 */
	virtual uint8_t* map() = 0;

	/**
	 * Unmap the previously mapped buffer.
	 *
	 * This method unmaps the previously mapped buffer. This will invalidate the
	 * mapped opaque pointer.
	 */
	virtual void unmap() = 0;
};

}