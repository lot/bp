#pragma once
#include "gpu.hpp"
#include <memory>
#include <vector>

namespace bp {

//Forward declarations

//bp/gpu/surface.hpp
class surface;

/**
 * GPU query for enumerating capable GPUs.
 *
 * This class represents GPU queries for which you can specify requirements for
 * GPUs and enumerate the capable GPUs that satisfies the requirements.
 */
class gpu_query {
	gpu_usage m_usage;
	std::shared_ptr<bp::surface> m_surface;

public:
	gpu_query() : m_usage{0} {}
	virtual ~gpu_query() {}

	/**
	 * Get the usage flags set for this query.
	 *
	 * Get the usage flags that must be supported by GPUs enumerated by this
	 * query.
	 * @return The usage flags.
	 */
	gpu_usage usage() const { return m_usage; }

	/**
	 * Set the usage flags for this query.
	 *
	 * Set the usage flags that must be supported by GPUs enumerated by this
	 * query.
	 * @param u The usage flags.
	 */
	void usage(gpu_usage u) { m_usage = u; }

	/**
	 * Get the surface it should be possible to present to.
	 *
	 * Get the surface that GPUs enumerated by this query should be capable of
	 * presenting to.
	 * @return The surface, or nullptr if no surface is set.
	 */
	std::shared_ptr<bp::surface> surface() { return m_surface; }

	/**
	 * Set the surface it should be possible to present to.
	 *
	 * Set the surface that GPUs enumerated by this query should be capable of
	 * presenting to.
	 * @param s The surface
	 */
	void surface(std::shared_ptr<bp::surface> s) {
		m_surface = s;
		if (s) m_usage |= gpu_usage::surface;
	}

	/**
	 * Get the list of capable GPUs.
	 *
	 * Get the list of GPUs that satisfies the requirements specified in this
	 * query.
	 * @return A vector of capable GPUs.
	 */
	virtual std::vector<gpu> results() = 0;
};

}