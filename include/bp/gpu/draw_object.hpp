#pragma once
#include <memory>
#include <vector>
#include <cstdint>

namespace bp {

class pipeline;
class buffer;
class texture;
enum class shader_stage;

struct vertex_buffer_binding {
	std::shared_ptr<bp::buffer> buffer;
	size_t offset;
};

enum class index_type {
	u16,
	u32
};

class draw_object {
public:
	virtual ~draw_object() = default;

	virtual void bind_pipeline(std::shared_ptr<bp::pipeline> pipeline) = 0;

	virtual void push_constants(
		uint32_t offset,
		uint32_t size,
		const void* data,
		std::vector<shader_stage> shader_stages
	) = 0;

	virtual void bind_vertex_buffers(
		std::vector<vertex_buffer_binding> bindings,
		uint32_t first_binding = 0
	) = 0;

	virtual void bind_index_buffer(
		std::shared_ptr<bp::buffer> buffer,
		size_t offset,
		bp::index_type index_type
	) = 0;

	virtual void draw(
		uint32_t vertex_count,
		uint32_t instance_count,
		uint32_t first_vertex,
		uint32_t first_instance
	) = 0;

	virtual void draw_indexed(
		uint32_t index_count,
		uint32_t instance_count,
		uint32_t first_index,
		int32_t vertex_offset,
		uint32_t first_instance
	) = 0;
};

}