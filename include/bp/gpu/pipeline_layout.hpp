#pragma once
#include <memory>
#include <cstdint>
#include <vector>

namespace bp {

enum class descriptor_type;
enum class shader_stage;

class pipeline_layout {
public:
	virtual ~pipeline_layout() = default;

	virtual void add_push_constant_range(
		std::vector<shader_stage> shader_stages,
		uint32_t offset,
		uint32_t size
	) = 0;

	virtual void add_descriptor_binding(
		uint32_t binding,
		std::vector<shader_stage> shader_stages,
		descriptor_type type,
		uint32_t count = 1
	) = 0;
};

}