#pragma once

namespace bp {

enum class descriptor_type {
	sampler,
	combined_image_sampler,
	sampled_image,
	storage_image,
	uniform_texel_buffer,
	storage_texel_buffer,
	uniform_buffer,
	storage_buffer,
	uniform_buffer_dynamic,
	storage_buffer_dynamic,
	input_attachment,
};

}