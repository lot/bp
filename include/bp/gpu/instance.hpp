#pragma once
#include <memory>

namespace bp {

//Forward declarations

//bp/gpu/gpu_query.hpp
class gpu_query;

//bp/gpu/gpu.hpp
class gpu;
class gpu_usage;

//bp/gpu/context.hpp
class context;

/**
 * Instance providing access to the bp GPU API.
 *
 * The instance is the entry point for utilizing the functionality of the bp
 * GPU API. From the instance you can create GPU queries to enumerate the
 * capable GPUs for your needs. Then you can create GPU contexts for setting up
 * resources and executing GPU commands.
 */
class instance {
public:
	virtual ~instance() {}

	/**
	 * Create a GPU query for enumerating capable GPUs.
	 *
	 * This method creates a GPU query that allows specifying needed GPU
	 * capability, and enumerating the capable GPUs.
	 *
	 * @return The created GPU query.
	 */
	virtual std::shared_ptr<gpu_query> create_gpu_query() = 0;

	/**
	 * Create a GPU context.
	 *
	 * This method creates a GPU context from which you can set up GPU
	 * resources like textures and buffers, as well as executing GPU commands.
	 *
	 * @param gpu The GPU the created context should operate on.
	 * @param usage Usage flags specifying what capability is needed.
	 * @return The created GPU context.
	 */
	virtual std::shared_ptr<context> create_context(
		const bp::gpu& gpu,
		gpu_usage usage
	) = 0;
};

#include <bp/util/api.hpp>

/**
 * Create an instance for the bp GPU API.
 *
 * This function creates an instance for accessing the functionality of the bp
 * GPU API.
 *
 * @return The created instance.
 */
BP_API std::shared_ptr<instance> create_instance();

#undef BP_API

}