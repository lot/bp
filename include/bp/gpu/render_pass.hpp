#pragma once
#include <memory>

namespace bp {

class attachment_flags;
class attachment;
enum class pipeline_stage;
class subpass;
enum class texture_format;
class swapchain;

class render_pass {
public:
	virtual ~render_pass() = default;

	virtual std::shared_ptr<attachment> add_attachment(
		texture_format format,
		attachment_flags flags
	) = 0;

	virtual std::shared_ptr<attachment> add_attachment(
		std::shared_ptr<bp::swapchain> swapchain,
		attachment_flags flags
	) = 0;

	virtual std::shared_ptr<subpass> add_subpass() = 0;
};

}