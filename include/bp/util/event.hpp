#pragma once
#include <unordered_map>
#include <functional>

namespace bp {

/*
 * Event representation for a simple event-delegate system.
 * Delegates are std::function objects.
 * Use the connect functions below, or the attach method to attach delegates.
 */
template<typename... ParamTypes>
class event {
	std::unordered_map<unsigned, std::function<void(ParamTypes...)>> delegates;
	unsigned next_id{0};

public:
	void operator()(ParamTypes... args) {
		for (auto& d : delegates)
			d.second(args...);
	}

	unsigned attach(std::function<void(ParamTypes...)> d) {
		delegates.insert({next_id, d});
		return next_id++;
	}

	void detach(unsigned id) {
		delegates.erase(id);
	}
};

/*
 * Attach a functor delegate.
 * The functor could be a function or a closure.
 */
template<typename... ParamTypes, typename Functor>
unsigned connect(event<ParamTypes...>& e, Functor f) {
	return e.attach(f);
}

/*
 * Forward an event a to b.
 */
template<typename... ParamTypes>
unsigned connect(event<ParamTypes...>& a, event<ParamTypes...>& b) {
	return a.attach([&b](ParamTypes... args) { b(args...); });
}

/*
 * Attach a method delegate for the object o.
 */
template<class T, typename... ParamTypes>
unsigned connect(event<ParamTypes...>& e, T& o, void(T::*m)(ParamTypes...)) {
	return e.attach([&o, m](ParamTypes... args) { (o.*m)(args...); });
}

}