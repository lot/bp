#pragma once
#include <vector>
#include <cstdint>
#include <string>
#include <sstream>

namespace bp {

extern const char path_separator;

const std::string& native_path(std::string& path);

std::string native_path(std::string path);

template <class None = void>
void path_rec(std::stringstream&) {}

template <typename Part>
void path_rec(std::stringstream& ss, Part part) {
	ss << part;
}

template <typename Part, typename... Remaining>
void path_rec(std::stringstream& ss, Part part, Remaining... remaining) {
	ss << part << path_separator;
	path_rec<Remaining...>(ss, remaining...);
}

template <typename... Parts>
std::string path(Parts... parts) {
	std::stringstream ss;
	path_rec(ss, parts...);
	return ss.str();
}

std::string base_path(const std::string& path);

std::string file_name(const std::string& path);

std::vector<std::uint8_t> load_binary_file(const std::string& path);

}