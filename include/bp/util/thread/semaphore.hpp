#pragma once
#include <mutex>
#include <condition_variable>

namespace bp {

class semaphore {
	unsigned n;
	std::mutex mutex;
	std::condition_variable condition_variable;

public:
	semaphore() : n{0} {};
	semaphore(unsigned n) : n{n} {}

	void notify() {
		std::unique_lock<std::mutex> lk{mutex};
		++n;
		condition_variable.notify_one();
	}

	void wait() {
		std::unique_lock<std::mutex> lk{mutex};
		while (n == 0) {
			condition_variable.wait(lk);
		}
		--n;
	}
};

}