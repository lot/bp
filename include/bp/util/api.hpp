#if !defined(BP_API)

#if defined(WIN32) || defined(_WIN32) || defined(__WIN32) && !defined(__CYGWIN__)
#if _MSC_VER && !__INTEL_COMPILER
#pragma warning(disable:4251)
#pragma warning(disable:4275)
#endif
#if defined(BP_API_IMPL)
#define BP_API __declspec(dllexport)
#else
#define BP_API __declspec(dllimport)
#endif

#else
#define BP_API
#endif

#endif